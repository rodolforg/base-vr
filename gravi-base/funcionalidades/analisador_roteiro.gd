extends Node

# emitted when the test starts
signal roteiro_iniciado
# emitted when the whole test finishes
signal roteiro_terminado
# emitted when a test step ends
signal passo_terminado
# emitted when a goal is shown
signal goal_shown

# test resource file name
export(String, FILE, "*.txt") var arquivo_roteiro : String
# identifier of initial step
export(String) var initial_step_id : String
# allow to start as soon as node is ready
export(bool) var autostart : bool = true
# allow to start next test step after one finishes
export(bool) var autocontinue : bool = true
# scenary node
export(NodePath) var world_path : NodePath = "Cenário"
export(NodePath) var player_path : NodePath = world_path.get_concatenated_subnames() + NodePath('/Jogador')

var world : Node
var player : Node

const DEFAULT_PLAYER_NODE_NAME = 'Jogador'
# Node for guiding arrow for command 'ChamaAtencao'
const GUIDING_ARROW_PATH : String = 'Jogador/ARVRCamera/seta_guia'
const DEFAULT_OFFSET_GUIDING_ARROW : Vector3 = Vector3(0, -.2, -1.0)

# Node for instruction message panel
const INSTRUCTION_PANEL_PATH = 'Jogador/ARVRCamera/painel_instrucoes'
# Node for goal_text message panel
const GOAL_PANEL_PATH = 'Jogador/ARVRCamera/painel_objetivo'
const DEFAULT_OFFSET_GOAL_PANEL : Vector3 = Vector3(0, 0, -2.0)

# Node for option panel
const OPTION_PANEL_PATH = 'Jogador/ARVRCamera/painel_opcoes'
const DEFAULT_OFFSET_OPTION_PANEL : Vector3 = Vector3(0, 0, -2.0)

const LEFT_HAND_PATH = 'Jogador/MaoEsquerda'
const RIGHT_HAND_PATH = 'Jogador/MaoDireita'

# a dictionary structure that contains test steps: each step is an array of TestCommands. Keys are step ids
var roteiro : TestData
# current running step
var current_step_id = null
# last running step
var last_step_id = null

var start_time

# last shown instruction
var current_instruction_text : String
# an array of extra commands to be executed before next designed command
var injected_commands = []

var command_classes = [
	ComandoPosiciona, ComandoOrienta,
	ComandoChamaAtencao, ComandoParadeChamarAtencao,
	ComandoAguardarChegar,
	ComandoAguardarSinal, ComandoEmitirSinal,
	ComandoExecutaComando, ComandoAlteraPropriedade,
	ComandoExibirInstrucao,
	ComandoAguardarPegar, ComandoAguardarTempo,
	ComandoAguardarSelecionar,
	ComandoExibirOpcoes, ComandoAguardarMarcarOpcoes
]

const GlowingMaterial = preload("res://gravi-base/funcionalidades/glowing.material")

onready var Highlighter = DestacadorObjeto.new()

class TestData:
	var goal_text
	var first_step_label : String
	var steps : Dictionary
	var stages : Dictionary
	var variables : Dictionary

func _ready():
	world = get_node(world_path)
	if not world:
		printerr("Não configurou o nó raiz do cenário")
		return
	if player_path:
		player = get_node(player_path)
	else:
		player = world.get_node(DEFAULT_PLAYER_NODE_NAME)
	if not player:
		printerr("Não configurou o nó do jogador")
		return
	elif not player is ARVROrigin:
		printerr("O nó do jogador deve ser do tipo ARVROrigin ou derivados")
		return
	roteiro = carregar_roteiro(arquivo_roteiro)
	if not roteiro:
		printerr("Erro ao carregar o teste")
		return
	var ok : int = connect("passo_terminado", self, "on_step_finished")
	if ok != OK:
		printerr("(Roteiro) Não conseguiu conectar ao sinal 'passo_terminado'")
	emit_signal('roteiro_iniciado')
	if roteiro.goal_text:
		yield(show_goal_and_wait(roteiro.goal_text), 'completed')
	start_time = OS.get_ticks_msec()
	if not initial_step_id:
		initial_step_id = roteiro.first_step_label
	
	if autostart:
		exec_test(initial_step_id, roteiro.variables)

func on_step_finished(step_id):
	print('step finished')
	if autocontinue:
		if not current_step_id:
			var next_step_idx = roteiro.steps.keys().find(step_id)+1
			if next_step_idx < roteiro.steps.size():
				step_id = roteiro.steps.keys()[next_step_idx]
				exec_test(step_id, roteiro.variables)
			else:
				emit_signal('roteiro_terminado')

func carregar_conteudo_arquivo(nome_arquivo : String) -> String:
	var arquivo : File = File.new()
	var ok = arquivo.open(nome_arquivo, File.READ)
	if ok != OK:
		printerr('Não foi possível abrir o arquivo de roteiro: ', arquivo_roteiro)
		return ''
	var conteudo : String = arquivo.get_as_text()
	arquivo.close()
	return conteudo

func create_test_tree(test_contents) -> TestData:
	var step_tree = {}
	var first_step_label

	var tree_text : TestData = parse_test_contents(test_contents)
	var step_labels = tree_text.steps.keys()
	var variables : Dictionary = {}

	for step_label in step_labels:
		var step_commands = []
		var minor_steps = tree_text.steps[step_label]
		for command_line in minor_steps:
			var command_text = command_line['text']

			var command_recognized = false
			for possible_command in command_classes:
				if possible_command.check(command_text):
					var command = possible_command.new()
					var ok = command.parse(command_text, variables)
					if not ok:
						var line_number = command_line['number']
						printerr("Error on parsing line #",line_number," : ", command_text)
					step_commands.append(command)
					command_recognized = true
					break
			if not command_recognized:
				var line_number = command_line['number']
				printerr("Unknown command (line #",line_number,") : ", command_text)
		step_tree[step_label] = step_commands
		if not first_step_label:
			first_step_label = step_label

	tree_text.steps = step_tree
	tree_text.first_step_label = first_step_label
	tree_text.variables = variables
	return tree_text

func parse_test_contents(test_contents : String) -> TestData:
	# convert windows/mac line-ending to unix
	test_contents = test_contents.replace("\r\n", "\n")
	test_contents = test_contents.replace("\r", "\n")

	var test_goal
	var test_stages = {}
	var test_dict = {}
	var current_test_step_label = null
	var current_test_step_commands = null

	var step_label_regex = RegEx.new()
	step_label_regex.compile('^PASSO\\s+(?<label>.*)')

	var stage_label_regex = RegEx.new()
	stage_label_regex.compile('^>\\s+ETAPA\\s*\\d+\\s*:\\s*(?<label>.*)')

	var goal_label_regex = RegEx.new()
	goal_label_regex.compile('^>>>\\s+OBJETIVO\\s*:\\s*(?<label>.*)')

	var test_lines = clean_test_contents(test_contents)

	var goal_result = goal_label_regex.search(test_lines[0].text)
	if goal_result:
		test_goal = goal_result.get_string('label')
		test_lines.pop_front()

	var previous_stage_line

	for line in test_lines:
		var label_result = step_label_regex.search(line.text)
		if label_result:
			var label = label_result.get_string('label')
			if current_test_step_label:
				# push_current_test_step_parsing()
				test_dict[current_test_step_label] = current_test_step_commands
			if previous_stage_line:
				test_stages[label] = previous_stage_line.text
				previous_stage_line = null
			# reset_current_test_step_parsing()
			current_test_step_label = label
			current_test_step_commands = []
		else:
			var stage_result = stage_label_regex.search(line.text)
			if stage_result:
				if previous_stage_line:
					printerr('Error on parsing test: Stage definition without contents.\n  Ignoring line #', previous_stage_line.number, ' : ', previous_stage_line.text)
				previous_stage_line = {'number': line.number, 'text':stage_result.get_string('label')}
				continue
			elif not current_test_step_label:
				printerr('Error on parsing test: text contents before any Step label definition line.\n  Ignoring line #', line.number, ' : ', line.text)
				continue
			var step_command_info = line
			current_test_step_commands.push_back(step_command_info)

	if current_test_step_label:
		# push_current_test_step_parsing()
		test_dict[current_test_step_label] = current_test_step_commands

	var test : TestData = TestData.new()
	test.steps = test_dict
	test.goal_text = test_goal
	test.stages = test_stages
	return test

func carregar_roteiro(nome_arquivo : String) -> TestData:
	if not nome_arquivo:
		printerr("Não foi definido um arquivo de teste para ser usado")
		return null
	var conteudo : String = carregar_conteudo_arquivo(nome_arquivo)
	return create_test_tree(conteudo)

func clean_test_contents(test_contents):
	var cleaned_version = []

	# convert windows/mac line-ending to unix
	test_contents = test_contents.replace("\r\n", "\n")
	test_contents = test_contents.replace("\r", "\n")

	var test_lines = test_contents.split("\n", true)

	var line_number = 0
	for test_line in test_lines:
		line_number += 1
		test_line = test_line.strip_edges()
		if test_line.empty():
			continue
		if test_line.begins_with('#'):
			continue
		var line_info = {'number':line_number, 'text':test_line}
		cleaned_version.push_back(line_info)

	return cleaned_version

func exec_test(step_id, variables : Dictionary):
	current_step_id = step_id
	if not roteiro.steps.has(current_step_id):
		printerr("Roteiro não contém o passo '", current_step_id, "'")
		printerr("Passo ignorado")
		return
	var step = roteiro.steps[current_step_id]
	for command in step:
		print("comando: ", command.COMMAND_NAME)
		var pause = command.exec(self, variables)
		while pause and pause.is_valid():
			yield(get_tree().create_timer(0.1), "timeout")

		var extra_command = injected_commands.pop_front()
		while extra_command:
			if not extra_command is BaseComando:
				printerr('comando extra inválido ignorado')
			else:
				print("comando extra: ", extra_command.COMMAND_NAME)
				pause = extra_command.exec(self, variables)
				while pause and pause.is_valid():
					yield(get_tree().create_timer(0.1), "timeout")
			extra_command = injected_commands.pop_front()

	last_step_id = current_step_id
	current_step_id = null
	emit_signal('passo_terminado', step_id)

func show_goal_and_wait(goal_text):
	print(goal_text)
	var goal_node = pega_ou_cria_painel_objetivo(world)
	goal_node.texto = goal_text
	goal_node.visible = true
	emit_signal("goal_shown", goal_text)
	yield(goal_node, 'fechou')
	goal_node.visible = false

func pega_ou_cria_painel_objetivo(mundo : Spatial) -> Spatial:
	var no : Spatial = mundo.get_node_or_null(GOAL_PANEL_PATH)
	if not no:
		var painel_ : PackedScene = preload("res://gravi-base/itens/painel_objetivo/painel_objetivo.tscn")
		if not painel_:
			printerr('Não foi possível encontrar o arquivo do painel objetivo')
			return null
		no = painel_.instance()
		no.name = 'painel_objetivo'

		var componentes_caminho : PoolStringArray = GOAL_PANEL_PATH.split('/')
		componentes_caminho.remove(componentes_caminho.size() - 1)
		var caminho = componentes_caminho.join('/')
		mundo.get_node(caminho).add_child(no)
		no.transform.origin = DEFAULT_OFFSET_GOAL_PANEL
	return no

func pega_ou_cria_painel_instrucoes(mundo : Spatial, caminho : String = "") -> Spatial:
	var no : Spatial = mundo.get_node_or_null(INSTRUCTION_PANEL_PATH)
	if not no:
		var painel_ : PackedScene = preload("res://gravi-base/itens/painel_instrucoes/painel_instrucoes.tscn")
		if not painel_:
			printerr('Não foi possível encontrar o arquivo do painel de instruções')
			return null
		no = painel_.instance()
		no.name = 'painel_instrucoes'
		
		var usou_caminho_padrao : bool = false
		if not caminho or not mundo.has_node(caminho):
			if caminho:
				printerr('Nó "', caminho, '" não existe em ', mundo.get_path())
			var componentes_caminho : PoolStringArray = INSTRUCTION_PANEL_PATH.split('/')
			componentes_caminho.remove(componentes_caminho.size() - 1)
			caminho = componentes_caminho.join('/')
			usou_caminho_padrao = true
		mundo.get_node(caminho).add_child(no)
		if usou_caminho_padrao:
			no.transform.origin = DEFAULT_OFFSET_GOAL_PANEL
	return no

func pega_ou_cria_painel_opcoes(mundo : Spatial, caminho : String = "") -> Spatial:
	var no : Spatial = mundo.get_node_or_null(OPTION_PANEL_PATH)
	if not no:
		var painel_ : PackedScene = preload("res://gravi-base/itens/painel_opcoes/painel_opcoes.tscn")
		if not painel_:
			printerr('Não foi possível encontrar o arquivo do painel de opções')
			return null
		no = painel_.instance()
		no.name = 'painel_opcoes'
		
		var usou_caminho_padrao : bool = false
		if not caminho or not mundo.has_node(caminho):
			if caminho:
				printerr('Nó "', caminho, '" não existe em ', mundo.get_path())
			var componentes_caminho : PoolStringArray = OPTION_PANEL_PATH.split('/')
			componentes_caminho.remove(componentes_caminho.size() - 1)
			caminho = componentes_caminho.join('/')
			usou_caminho_padrao = true
		mundo.get_node(caminho).add_child(no)
		if usou_caminho_padrao:
			no.transform.origin = DEFAULT_OFFSET_OPTION_PANEL
	return no

func pega_ou_cria_seta_guia(mundo : Spatial) -> Spatial:
	var no : Spatial = mundo.get_node_or_null(GUIDING_ARROW_PATH)
	if not no:
		var seta_ : PackedScene = preload("res://gravi-base/itens/seta_guia/seta_guia.tscn")
		if not seta_:
			printerr('Não foi possível encontrar o arquivo da seta guia')
			return null
		no = seta_.instance()
		no.name = 'seta_guia'
		var componentes_caminho : PoolStringArray = GUIDING_ARROW_PATH.split('/')
		componentes_caminho.remove(componentes_caminho.size() - 1)
		var caminho = componentes_caminho.join('/')
		mundo.get_node(caminho).add_child(no)
		no.transform.origin = DEFAULT_OFFSET_GUIDING_ARROW
	return no

class BaseComando:
	func _init(line = '', variables = null):
		if line:
			if not parse(line, variables):
				printerr('erro ao analisar a linha "', line, '"')

	func parse(_line : String, _variables : Dictionary) -> bool:
		return false

	func exec(_control_node : Node, _variables : Dictionary):
		pass

	static func check(_line):
		return false

	static func get_node_name(node):
		if typeof(node) == TYPE_NODE_PATH:
			return node
		elif typeof(node) == TYPE_STRING:
			return node
		elif node:
			return node.name
		else:
			return null

	static func get_node(control_node : Node, variables : Dictionary, node_name, var_name) -> Node:
		var node
		if var_name:
			if variables.has(var_name):
				node = variables[var_name]
			else:
				printerr('variável inexistente: ', var_name)
		else:
			node = control_node.world.get_node(node_name)
		if not node:
			return null
		return node

class ComandoPosiciona:
	extends BaseComando

	var pos
	var var_node_name
	var node_name
	const COMMAND_NAME = 'posiciona'

	func _init(p_object = null, p_pos = null):
		self.pos = p_pos
		self.node_name = get_node_name(p_object)

	func parse(line : String, variables : Dictionary) -> bool:
		var regex = RegEx.new()
		regex.compile(COMMAND_NAME + "\\s+(\"(?<obj>[^\"]+)\"|<(?<var_name>[^>]+)>)\\s+(?<x>\\d+(\\.\\d*)?)\\s*,\\s*(?<y>\\d+(\\.\\d*)?)\\s*,\\s*(?<z>\\d+(\\.\\d*)?)")
		var result = regex.search(line)
		if result:
			if result.get_string("obj"):
				node_name = './' + result.get_string("obj")
			elif result.get_string("var_name"):
				var_node_name = result.get_string("var_name")
				if not variables.has(var_node_name):
					printerr('variável ainda não definida: "%s"' % var_node_name)
					return false
		else:
			printerr('sintaxe inválida para comando "%s"' % COMMAND_NAME)
			return false
		pos = Vector3(result.get_string("x"), result.get_string("y"), result.get_string("z"))
		return true

	func exec(control_node : Node, variables : Dictionary):
		var node = get_node(control_node, variables, node_name, var_node_name)
		if not node:
			printerr(('nó inexistente no comando "%s": "'+node_name) % COMMAND_NAME)
			return
		node.translation = pos

	static func check(line):
		return line.to_lower().begins_with('posiciona ')

class ComandoOrienta:
	extends BaseComando

	var rotation
	var var_node_name
	var node_name
	const COMMAND_NAME = 'orienta'

	func _init(p_object = null, p_rotation = null):
		self.rotation = p_rotation
		self.node_name = get_node_name(p_object)

	func parse(line : String, variables : Dictionary) -> bool:
		var regex = RegEx.new()
		regex.compile(COMMAND_NAME + "\\s+(\"(?<obj>[^\"]+)\"|<(?<var_name>[^>]+)>)\\s+(?<x>\\d+(\\.\\d*)?)\\s*,\\s*(?<y>\\d+(\\.\\d*)?)\\s*,\\s*(?<z>\\d+(\\.\\d*)?)")
		var result = regex.search(line)
		if result:
			if result.get_string("obj"):
				node_name = './' + result.get_string("obj")
			elif result.get_string("var_name"):
				var_node_name = result.get_string("var_name")
				if not variables.has(var_node_name):
					printerr('variável ainda não definida: "%s"', var_node_name)
					return false
		else:
			printerr('sintaxe inválida para comando "%s"' % COMMAND_NAME)
			return false
		node_name = './' + result.get_string("obj")
		rotation = Vector3(result.get_string("x"), result.get_string("y"), result.get_string("z"))
		return true

	func exec(control_node : Node, variables : Dictionary):
		var node : Spatial = get_node(control_node, variables, node_name, var_node_name)
		if not node:
			printerr(('nó inexistente no comando "%s": "'+node_name) % COMMAND_NAME)
			return
		node.rotation_degrees = rotation

	static func check(line):
		return line.to_lower().begins_with('orienta ')

class ComandoChamaAtencao:
	extends BaseComando

	var var_node_name
	var node_name
	var time_in_seconds = null
	const COMMAND_NAME = 'chama atenção'

	func _init(object = null, time = 0):
		self.time_in_seconds = time
		self.node_name = get_node_name(object)

	func parse(line : String, variables : Dictionary) -> bool:
		var regex = RegEx.new()
		regex.compile(COMMAND_NAME + "\\s+(de|do|da|para)?\\s+(\"(?<obj>[^\"]+)\"|<(?<var_name>[^>]+)>)")
		var result = regex.search(line)
		if result:
			if result.get_string("obj"):
				node_name = './' + result.get_string("obj")
			elif result.get_string("var_name"):
				var_node_name = result.get_string("var_name")
				if not variables.has(var_node_name):
					printerr('variável ainda não definida: "%s"', var_node_name)
					return false
		else:
			printerr('sintaxe inválida para comando "%s"' % COMMAND_NAME)
			return false
		node_name = './' + result.get_string("obj")
		var main_pattern_end = result.get_end()
		regex.compile("\\s+por\\s+(?<time>\\d+(\\.\\d*)?)\\s*(?<unity>(m|min|s|seg|ms))")
		var duration_result = regex.search(line.substr(main_pattern_end, line.length() - main_pattern_end))
		if duration_result:
			var time = duration_result.get_string("time")
			var unity = duration_result.get_string("unity")
			time_in_seconds = float(time)
			if unity == 's' || unity == 'sec':
				pass
			elif unity == 'ms':
				time_in_seconds /= 1000.0
			elif unity == 'm' || unity == 'min':
				time_in_seconds *= 60
		return true

	func exec(control_node : Node, variables : Dictionary):
		var target_node = get_node(control_node, variables, node_name, var_node_name)

		control_node.Highlighter.destacar(target_node)

		if time_in_seconds:
			yield(target_node.get_tree().create_timer(time_in_seconds), "timeout")
			control_node.Highlighter.retirar_destaque(target_node)

		var seta_guia = control_node.pega_ou_cria_seta_guia(control_node.world)
		seta_guia.visible = true
		seta_guia.alvo = target_node

	static func check(line):
		return line.to_lower().begins_with('chama atenção ')

class ComandoParadeChamarAtencao:
	extends BaseComando

	var var_node_name
	var node_name
	const COMMAND_NAME = 'para de chamar atenção'

	func _init(object = null):
		self.node_name = get_node_name(object)

	func parse(line : String, variables : Dictionary) -> bool:
		var regex = RegEx.new()
		regex.compile(COMMAND_NAME + "\\s+(de|do|da|para)?\\s+(\"(?<obj>[^\"]+)\"|<(?<var_name>[^>]+)>)")
		var result = regex.search(line)
		if result:
			if result.get_string("obj"):
				node_name = './' + result.get_string("obj")
			elif result.get_string("var_name"):
				var_node_name = result.get_string("var_name")
				if not variables.has(var_node_name):
					printerr('variável ainda não definida: "%s"', var_node_name)
					return false
		else:
			printerr('sintaxe inválida para comando "%s"' % COMMAND_NAME)
			return false
		node_name = './' + result.get_string("obj")
		return true

	func exec(control_node : Node, variables : Dictionary):
		var target_node = get_node(control_node, variables, node_name, var_node_name)

		control_node.Highlighter.retirar_destaque(target_node)

		var seta_guia = control_node.pega_ou_cria_seta_guia(control_node.world)
		seta_guia.visible = false
		seta_guia.alvo = null

	static func check(line):
		return line.to_lower().begins_with('para de chamar atenção ')

class ComandoAguardarSinal:
	extends BaseComando

	var var_node_name
	var node_name
	var signal_name
	const COMMAND_NAME = 'aguarda sinal'

	func _init(p_object = null, p_signal_name = null):
		self.signal_name = p_signal_name
		self.node_name = get_node_name(p_object)

	func parse(line : String, variables : Dictionary) -> bool:
		var regex = RegEx.new()
		regex.compile(COMMAND_NAME + "\\s+\"(?<signal>[^\"]+)\"\\s+(de|do|da)\\s+(\"(?<obj>[^\"]+)\"|<(?<var_name>[^>]+)>)")
		var result = regex.search(line)
		if result:
			if result.get_string("obj"):
				node_name = './' + result.get_string("obj")
			elif result.get_string("var_name"):
				var_node_name = result.get_string("var_name")
				if not variables.has(var_node_name):
					printerr('variável ainda não definida: "%s"', var_node_name)
					return false
		else:
			printerr('sintaxe inválida para comando "%s"' % COMMAND_NAME)
			return false
		signal_name = result.get_string("signal")
		return true

	func exec(control_node : Node, variables : Dictionary):
		var node = get_node(control_node, variables, node_name, var_node_name)

		print('\tesperando sinal "%s" de "%s"' % [signal_name, node.get_name()])
		yield(node, signal_name)
		print('\trecebido')

	static func check(line):
		return line.to_lower().begins_with('aguarda sinal ')

class ComandoEmitirSinal:
	extends BaseComando

	var var_node_name
	var node_name
	var signal_name
	const COMMAND_NAME = 'emite sinal'

	func _init(p_object = null, p_signal_name = null):
		self.signal_name = p_signal_name
		self.node_name = get_node_name(p_object)

	func parse(line : String, variables : Dictionary) -> bool:
		var regex = RegEx.new()
		regex.compile(COMMAND_NAME + "\\s+\"(?<signal>[^\"]+)\"\\s+(de|do|da)\\s+(\"(?<obj>[^\"]+)\"|<(?<var_name>[^>]+)>)")
		var result = regex.search(line)
		if result:
			if result.get_string("obj"):
				node_name = './' + result.get_string("obj")
			elif result.get_string("var_name"):
				var_node_name = result.get_string("var_name")
				if not variables.has(var_node_name):
					printerr('variável ainda não definida: "%s"', var_node_name)
					return false
		else:
			printerr('sintaxe inválida para comando "%s"' % COMMAND_NAME)
			return false
		node_name = './' + result.get_string("obj")
		signal_name = result.get_string("signal")
		return true

	func exec(control_node : Node, variables : Dictionary):
		var node = get_node(control_node, variables, node_name, var_node_name)
		node.emit_signal(signal_name)

	static func check(line):
		return line.to_lower().begins_with('emite sinal ')

# TODO accept more parameters
class ComandoExecutaComando:
	extends BaseComando

	var var_node_name
	var node_name
	var node_command_name
	var arg1 = null
	const COMMAND_NAME = 'executa comando'

	func _init(p_object = null, p_commandName = null, p_arg1 = null):
		self.node_command_name = p_commandName
		self.arg1 = p_arg1
		self.node_name = get_node_name(p_object)

	func parse(line : String, variables : Dictionary) -> bool:
		var regex = RegEx.new()
		regex.compile(COMMAND_NAME + "\\s+\"(?<command>[^\"]+)\"\\s+(de|do|da)\\s+(\"(?<obj>[^\"]+)\"|<(?<var_name>[^>]+)>)(\\s*:\\s*\"(?<arg1>[^\"]*)\")?")
		var result = regex.search(line)
		if result:
			if result.get_string("obj"):
				node_name = './' + result.get_string("obj")
			elif result.get_string("var_name"):
				var_node_name = result.get_string("var_name")
				if not variables.has(var_node_name):
					printerr('variável ainda não definida: "%s"', var_node_name)
					return false
		else:
			printerr('sintaxe inválida para comando "%s"' % COMMAND_NAME)
			return false
		node_name = './' + result.get_string("obj")
		node_command_name = result.get_string("command")
		arg1 = result.get_string("arg1")
		return true

	func exec(control_node : Node, variables : Dictionary):
		var node = get_node(control_node, variables, node_name, var_node_name)
		if arg1:
			node.call(node_command_name, arg1)
		else:
			node.call(node_command_name)

	static func check(line):
		return line.to_lower().begins_with('executa comando ')

class ComandoExibirInstrucao:
	extends BaseComando

	var instruction_text : String = ""
	var spawn_node_path : String = ""
	const COMMAND_NAME = 'exibe instrução'

	func _init(p_instruction_text : String = "", p_spawn_node_path : String = ""):
		self.instruction_text = p_instruction_text
		self.spawn_node_path = p_spawn_node_path

	func parse(line : String, _variables : Dictionary) -> bool:
		var regex = RegEx.new()
		regex.compile(COMMAND_NAME + "\\s+(\"(?<instruction_text>[^\"]*)\")(\\s+em\\s+\"(?<node>[^\"]+)\")?")
		var result = regex.search(line)
		if not result:
			printerr('sintaxe inválida para comando "%s"' % COMMAND_NAME)
			return false
		instruction_text = result.get_string("instruction_text")
		spawn_node_path = result.get_string("node")
		return true

	func exec(control_node : Node, _variables : Dictionary):
		var node = control_node.pega_ou_cria_painel_instrucoes(control_node.world, spawn_node_path)
		node.visible = true
		var elapsed_time = OS.get_ticks_msec() - control_node.start_time
		var minutes = floor(elapsed_time/60000.0)
		var seconds = round(elapsed_time/1000.0 - minutes * 60)
		var parsed_text = instruction_text
		parsed_text = parsed_text.replace('[MINUTOS]', str(minutes))
		parsed_text = parsed_text.replace('[SEGUNDOS]', str(seconds))
		parsed_text = parsed_text.replace('\\n', '\n')
		#parsed_text = parsed_text.replace('[NOME]', Sessao.participante_atual)
		node.set_texto_principal(parsed_text)
		node.ignora_usuario_fechar = false
		if not node.is_aberto():
			node.abrir()
		control_node.current_instruction_text = parsed_text
		print('\tesperando sinal "fechou" de "%s"' % node.name)
		yield(node, "fechou")
		print('\trecebido')

	static func check(line):
		return line.to_lower().begins_with('exibe instrução ')

class ComandoAlteraPropriedade:
	extends BaseComando

	var var_node_name : String
	var node_name : NodePath
	var property_name
	var value = null
	const COMMAND_NAME = 'muda valor'

	func _init(p_object = null, p_property_name = null, p_value = null):
		self.property_name = p_property_name
		self.value = p_value
		if p_object:
			self.node_name = p_object.name

	func parse(line : String, variables : Dictionary) -> bool:
		var regex = RegEx.new()
		regex.compile(COMMAND_NAME + "\\s+\"(?<property>[^\"]+)\"\\s+(de|do|da)\\s+(\"(?<obj>[^\"]+)\"|<(?<var_name>[^>]+)>)(\\s*:\\s*(?<value>[^\"]*)(#.*)?)")
		var result = regex.search(line)
		if result:
			if result.get_string("obj"):
				node_name = './' + result.get_string("obj")
			elif result.get_string("var_name"):
				var_node_name = result.get_string("var_name")
				if not variables.has(var_node_name):
					printerr('variável ainda não definida: "%s"', var_node_name)
					return false
		else:
			printerr('sintaxe inválida para comando "%s"' % COMMAND_NAME)
			return false
		node_name = './' + result.get_string("obj")
		property_name = result.get_string("property")
		value = result.get_string("value")
		return true

	func exec(control_node : Node, variables : Dictionary):
		var node = get_node(control_node, variables, node_name, var_node_name)
		for prop in node.get_property_list():
			if prop.name == property_name:
				match prop.type:
					TYPE_BOOL:
						value = bool(value)
					TYPE_INT:
						value = int(value)
					TYPE_REAL:
						value = float(value)
					TYPE_COLOR:
						value = Color(value)
		node.set(property_name, value)

	static func check(line):
		return line.to_lower().begins_with('muda valor ')

# aguarda chegar em "objeto"
# aguarda chegar em <variavel>
# aguarda chegar em x, y, z # usar ponto . como separador decimal
class ComandoAguardarChegar:
	extends BaseComando

	var var_node_name : NodePath
	var node_name : NodePath
	var pos : Vector3
	const COMMAND_NAME = 'aguarda chegar em'
	
	var recurso_area = preload("res://gravi-base/funcionalidades/area-deteccao-jogador/area_deteccao_jogador.tscn")

	func _init(p_pos = null):
		if p_pos is Vector3:
			self.pos = p_pos
		elif p_pos is NodePath:
			self.node_name = p_pos
		elif p_pos is Spatial:
			self.node_name = get_node_name(p_pos)

	func parse(line : String, variables : Dictionary) -> bool:
		var regex = RegEx.new()
		regex.compile(COMMAND_NAME + "\\s+(\"(?<obj>[^\"]+)\"|<(?<var_name>[^>]+)>|(?<x>-?\\d+(\\.\\d*)?)\\s*,\\s*(?<y>-?\\d+(\\.\\d*)?)\\s*,\\s*(?<z>-?\\d+(\\.\\d*)?))")
		var result = regex.search(line)
		if result:
			if result.get_string("obj"):
				node_name = './' + result.get_string("obj")
			elif result.get_string("var_name"):
				var_node_name = result.get_string("var_name")
				if not variables.has(var_node_name):
					printerr('variável ainda não definida: "%s"', var_node_name)
					return false
			elif result.get_string("x"):
				if not result.get_string("y") or not result.get_string("z"):
					printerr('posição precisa as três coordenadas')
					return false
				else:
					pos = Vector3(result.get_string("x"), result.get_string("y"), result.get_string("z"))
		else:
			printerr('sintaxe inválida para comando "%s"' % COMMAND_NAME)
			return false
		return true

	func exec(control_node : Node, variables : Dictionary):
		var area_pos : Vector3
		var node : Spatial
		if not node_name and not var_node_name:
			area_pos = pos
		else:
			node = get_node(control_node, variables, node_name, var_node_name)
			if node:
				area_pos = node.global_transform.origin
			else:
				printerr('nó não encontrado: "%s"', node_name if node_name else var_node_name)
				return
		
		var area : Area = recurso_area.instance()
		area.monitoring = false
		if node:
			node.add_child(area)
		else:
			control_node.add_child(area)
			area.global_transform.origin = area_pos
		area.monitoring = true
		
		var no_movimentacao_jogador : Spatial = control_node.player.get_node("Movimentação")
		var jogador_estava_com_colisao : bool = no_movimentacao_jogador.enableCollision
		no_movimentacao_jogador.enableCollision = true

		print('\tesperando jogador chegar em "%s"' % [area_pos])
		yield(area, 'jogador_entrou')
		print('\tchegou')
		control_node.remove_child(area)
		area.queue_free()
		
		no_movimentacao_jogador.enableCollision = jogador_estava_com_colisao

	static func check(line):
		return line.to_lower().begins_with('aguarda chegar em ')

class ComandoAguardarPegar:
	extends BaseComando

	var group
	var var_node_name
	var node_name
	var object
	var left_hand_node
	var right_hand_node
	var assign_var
	const COMMAND_NAME = 'aguarda pegar'

	class WatchNode :
		extends Node
		var object
		var group

		signal pegou(object)

		func _init(p_object, p_group):
			self.object = p_object
			self.group = p_group

		func espera_pegar_o_correto(obj):
			if (group and obj.is_in_group(group)) or (object and object == obj):
				emit_signal("pegou", obj)

	func _init(p_group = null):
		self.group = p_group

	func parse(line : String, variables : Dictionary) -> bool:
		var regex = RegEx.new()
		regex.compile(COMMAND_NAME + "\\s+(algum[a]? \"(?<group>[^\"]+)\"|(\"(?<obj>[^\"]+)\"|<(?<var_name>[^>]+)>))(\\s+->\\s+<(?<var_assignment>[^>]+)>)?")
		var result = regex.search(line)
		if result:
			if result.get_string("group"):
				group = result.get_string("group")
			elif result.get_string("obj"):
				node_name = './' + result.get_string("obj")
			elif result.get_string("var_name"):
				var_node_name = result.get_string("var_name")
				if not variables.has(var_node_name):
					printerr('variável ainda não definida: "%s"', var_node_name)
					return false
		else:
			printerr('sintaxe inválida para comando "%s"' % COMMAND_NAME)
			return false
		assign_var = result.get_string("var_assignment")
		if assign_var:
			variables[assign_var] = null
		return true

	func exec(control_node : Node, variables : Dictionary):
		if group:
			print('\tesperando pegar algum elemento do grupo "%s"' % group)
		else:
			if object:
				print('\tesperando pegar "%s"' % object.get_name())
			else:
				print('\tesperando pegar "%s"' % node_name)
			object = get_node(control_node, variables, node_name, var_node_name)

		left_hand_node = control_node.world.get_node(LEFT_HAND_PATH)
		right_hand_node = control_node.world.get_node(RIGHT_HAND_PATH)

		if check_hand_contents(left_hand_node):
			variables[assign_var] = left_hand_node.objeto_segurado
		elif check_hand_contents(right_hand_node):
			variables[assign_var] = right_hand_node.objeto_segurado
		else:
			var extra_node = create_extra_node(object, group)
			var obj = yield(extra_node, "pegou")
			variables[assign_var] = obj
		print('\tpegou')

	func create_extra_node(p_object, p_group):
		var extra_node : WatchNode = WatchNode.new(p_object, p_group)
		extra_node.add_user_signal("pegou", [{"objeto":TYPE_OBJECT}])
		var ok = left_hand_node.connect("pegou", extra_node, "espera_pegar_o_correto")
		if ok != OK:
			printerr('Não conectou "pegou" na mão esquerda')
		ok = right_hand_node.connect("pegou", extra_node, "espera_pegar_o_correto")
		if ok != OK:
			printerr('Não conectou "pegou" na mão direita')
		return extra_node

	func check_hand_contents(hand : Object) -> bool:
		var obj = hand.objeto_segurado
		if obj and ((object and object == obj) or (group and obj.is_in_group(group))):
			return true
		return false

	static func check(line):
		return line.to_lower().begins_with('aguarda pegar ')

class ComandoAguardarTempo:
	extends BaseComando

	var time_str
	var time
	const COMMAND_NAME = 'aguarda tempo'

	func _init(p_time = null):
		time = p_time

	func parse(line : String, _variables : Dictionary) -> bool:
		var regex = RegEx.new()
		regex.compile(COMMAND_NAME + "\\s+\"?(?<time>[^\"]+)\"?")
		var result = regex.search(line)
		if result:
			if result.get_string("time"):
				time_str = result.get_string("time")
		else:
			printerr('sintaxe inválida para comando "%s"' % COMMAND_NAME)
			return false

		regex.compile("(?<min>[0-9]+min)?(?<sec>[0-9]{0,2}s)?")
		result = regex.search(time_str)
		if result:
			time = 0
			if not result.get_string("min") and not result.get_string("sec"):
				printerr('nenhuma unidade de tempo definida em "%s"' % COMMAND_NAME)
			if result.get_string("min"):
				time += int(result.get_string("min")) * 60
			if result.get_string("sec"):
				time += int(result.get_string("sec"))
		else:
			printerr('sintaxe inválida para tempo em "%s"' % COMMAND_NAME)
			return false
		return true

	func exec(control_node : Node, _variables : Dictionary):
		print('\tesperando tempo de ', time_str, ' (', time, 's)')
		yield(control_node.get_tree().create_timer(time), "timeout")
		print('\tacabou')

	static func check(line):
		return line.to_lower().begins_with('aguarda tempo ')

# aguarda selecionar "algo"
# aguarda selecionar "algo" e "outro"
# aguarda selecionar dentre "grupo" "algo" e "outro"
class ComandoAguardarSelecionar:
	extends BaseComando
	const COMMAND_NAME = 'aguarda selecionar'

	var group : String
	var node_name_list : Array
	var objs : Array
	var laser : Spatial
	var laser_created_by_me : bool = false
	var left_hand_node : Spatial
	var right_hand_node : Spatial
	
	var select_object_once : bool = false
	
	var audio_player : AudioStreamPlayer
	var audio_acerto : AudioStream = preload("res://gravi-base/sons/acertou.wav")
	var audio_erro : AudioStream = preload("res://gravi-base/sons/errou.wav")

	func _init(p_group = ''):
		self.group = p_group

	func parse(line : String, _variables : Dictionary) -> bool:
		node_name_list = []
		objs = []
		
		var regex = RegEx.new()
		regex.compile(COMMAND_NAME + "\\s+(dentre? \"(?<grupo>[^\"]+)\"\\s+)?\"(?<objs>.+)\"")
		var result = regex.search(line)
		if result:
			if result.get_string("grupo"):
				group = result.get_string("grupo")
			if result.get_string("objs"):
				var nomes_objs = result.get_string("objs").split('" e "', false)
				for nome_obj in nomes_objs:
					node_name_list.push_back(NodePath("./" + nome_obj.strip_edges()))
		else:
			printerr('sintaxe inválida para comando "%s"' % COMMAND_NAME)
			return false
		
		return true

	func exec(control_node : Node, _variables : Dictionary):
		objs = []
		
		if group:
			if node_name_list.size() == 0: 
				print('\tesperando selecionar algum elemento do grupo "%s"' % group)
			else:
				print('\tesperando selecionar %d elementos do grupo "%s"' % [node_name_list.size(), group])

		var texto_nomes : String = ''
		for nome_obj in node_name_list:
			var obj : Spatial = control_node.world.get_node(nome_obj)
			if not obj:
				printerr("Nó não encontrado: ", nome_obj, ". Ignorando.")
			else:
				objs.push_back(obj)
				texto_nomes += String(nome_obj) + ', '
				if group:
					if not obj.is_in_group(group):
						printerr('Objeto a ser selecionado não está no grupo %s: %s' % [group, nome_obj])
		texto_nomes = texto_nomes.substr(0, texto_nomes.length() - 2)
		if objs:
			print('\tesperando selecionar "%s"' % texto_nomes)

		# Procura o laser indicador
		[laser, laser_created_by_me] = procura_laser_indicador(control_node.world)
		
		# Configura o laser indicador
		laser.grupo_alvo = group
		laser.visible = true
		
		if not audio_player:
			audio_player = AudioStreamPlayer.new()
			audio_player.stop()
			audio_player.name = 'no_audio_player_acerto_erro'
			control_node.add_child(audio_player)
		
		var selecionados : Array = []

		for obj in objs:
			var subcomando : SubComandoAguardarSelecionar = SubComandoAguardarSelecionar.new()
			subcomando.selecionados = selecionados
			subcomando.objs_selecionaveis = objs
			subcomando.laser = laser
			subcomando.laser_created_by_me = laser_created_by_me
			subcomando.audio_player = audio_player
			subcomando.audio_erro = audio_erro
			subcomando.audio_acerto = audio_acerto
			subcomando.seleciona_objeto_uma_vez = select_object_once
			control_node.injected_commands.push_back(subcomando)

	func procura_laser_indicador(mundo : Node) -> Array:
		left_hand_node = mundo.get_node(LEFT_HAND_PATH)
		right_hand_node = mundo.get_node(RIGHT_HAND_PATH)

		for no_filho in left_hand_node.get_children():
			if no_filho.has_user_signal('confirmou_alvo'):
				laser = no_filho
		if not laser:
			for no_filho in right_hand_node.get_children():
				if no_filho.has_user_signal('confirmou_alvo'):
					laser = no_filho
		if not laser:
			laser = preload("res://gravi-base/funcionalidades/laser-indicador/laser_indicador.tscn").instance()
			right_hand_node.add_child(laser)
			laser_created_by_me = true
		return [laser, laser_created_by_me]

	static func check(line):
		return line.to_lower().begins_with('aguarda selecionar ')

	class SubComandoAguardarSelecionar:
		extends BaseComando
		const COMMAND_NAME = 'aguarda selecionar (sub) '
	
		var selecionados : Array
		var objs_selecionaveis : Array
		var laser : Spatial
		var laser_created_by_me : bool
		var seleciona_objeto_uma_vez : bool = false
		
		var audio_player : AudioStreamPlayer
		var audio_acerto : AudioStream
		var audio_erro : AudioStream
		
		func exec(control_node : Node, _variables : Dictionary):
			yield(laser, "confirmou_alvo")
			if laser.alvo_confirmado in objs_selecionaveis:
				if not laser.alvo_confirmado in selecionados:
					# não seleciona de novo
					if seleciona_objeto_uma_vez:
						laser.alvo_confirmado.remove_from_group(laser.grupo_alvo)
					selecionados.push_back(laser.alvo_confirmado)
					print('\tselecionou "', laser.alvo_confirmado.name, '" (', laser.alvo_confirmado, ')')
					_toca_audio(audio_acerto)
					laser.alvo_confirmado = null
					if selecionados.size() == objs_selecionaveis.size():
						print('\tselecionou tudo!')
						var grupo_alvo : String = laser.grupo_alvo
						laser.visible = false
						if laser_created_by_me:
							laser.get_parent().remove_child(laser)
							laser.queue_free()
							laser = null
						if seleciona_objeto_uma_vez:
							for obj in selecionados:
								obj.add_to_group(grupo_alvo)
				else:
					print('\t\tjá havia selecionado "', laser.alvo_confirmado.name, '" (', laser.alvo_confirmado, ')')
					laser.alvo_confirmado = null
					control_node.injected_commands.push_back(self)
			else:
				# não seleciona de novo
				if seleciona_objeto_uma_vez:
					laser.alvo_confirmado.remove_from_group(laser.grupo_alvo)
				print('\tselecionou errado "', laser.alvo_confirmado.name, '" (', laser.alvo_confirmado, ')')
				laser.alvo_confirmado = null
				_toca_audio(audio_erro)
				control_node.injected_commands.push_back(self)
			
		func _toca_audio(audio : AudioStream) -> void:
			if not audio_player:
				print_debug('Sem nó reprodutor de som')
			else:
				audio_player.stream = audio
				audio_player.play()

# exibe opções "opção 1", "opção 2"
# exibe opções "opção 1", "opção 2", "opção 3"
# ...
# exibe opções "opção 1", "opção 2" em "caminho/no/referencia/para/posicao/do/painel"
class ComandoExibirOpcoes:
	extends BaseComando

	var opcoes : Array = []
	var caminho_no_referencia : String = ""
	const COMMAND_NAME = 'exibe opções'

	func _init(p_opcoes : Array = [], p_caminho_no_referencia : String = ""):
		self.opcoes = p_opcoes
		self.caminho_no_referencia = p_caminho_no_referencia

	func parse(line : String, _variables : Dictionary) -> bool:
		var regex = RegEx.new()
		regex.compile("\"\\s+em\\s+\"(?<no>[^\"]+)\"")
		var result : RegExMatch = regex.search(line, len(COMMAND_NAME))
		if not result:
			caminho_no_referencia = ""
		else:
			caminho_no_referencia = result.get_string("no")
		
		var posicao_fim : int = -1
		if result:
			posicao_fim = result.get_start() + 1
		regex.compile("\\s+\"(?<opcao>[^\"]+)\"\\s*,?")
		var lista_resultados = regex.search_all(line, len(COMMAND_NAME), posicao_fim)
		if not lista_resultados:
			printerr('sintaxe inválida para comando "%s"' % COMMAND_NAME)
			return false
		opcoes = []
		for resultado in lista_resultados:
			opcoes.push_back(resultado.get_string('opcao'))
		return true

	func exec(control_node : Node, variables : Dictionary):
		var node = control_node.pega_ou_cria_painel_opcoes(control_node.world, caminho_no_referencia)
		node.opcoes = opcoes
		node.visible = true
		variables['__no_painel_opcoes_controle_aberto__'] = node
		print('\texibindo opcoes:', opcoes)

	static func check(line):
		return line.to_lower().begins_with('exibe opções ')

# aguarda marcar x "opção correta 1"
# aguarda marcar x "opção correta 1", "opção correta 2"
# ...
class ComandoAguardarMarcarOpcoes:
	extends BaseComando

	var opcoes_corretas : Array = []
	var caminho_no_painel : String = ""
	const COMMAND_NAME = 'aguarda marcar x'

	func _init(p_opcoes_corretas : Array = [], p_caminho_no_painel : String = ""):
		self.opcoes_corretas = p_opcoes_corretas
		self.caminho_no_painel = p_caminho_no_painel

	func parse(line : String, _variables : Dictionary) -> bool:
		var regex = RegEx.new()
		regex.compile("\\s+\"(?<opcao>[^\"]+)\"")
		var lista_resultados = regex.search_all(line, len(COMMAND_NAME))
		if not lista_resultados:
			printerr('sintaxe inválida para comando "%s"' % COMMAND_NAME)
			return false
		opcoes_corretas = []
		for resultado in lista_resultados:
			opcoes_corretas.push_back(resultado.get_string('opcao'))
		return true

	func exec(control_node : Node, variables : Dictionary):
		if not variables.has('__no_painel_opcoes_controle_aberto__'):
			printerr('Comando "aguarda marcar x" chamado sem ter exibido painel de opções antes". Ignorado.')
			return
		var no_painel_opcoes = variables['__no_painel_opcoes_controle_aberto__']
		no_painel_opcoes.opcoes_corretas = opcoes_corretas
		print('\taguardando marcar opcoes:', opcoes_corretas)
		
		var mundo = control_node.world
		var right_hand_node : Spatial = mundo.get_node(RIGHT_HAND_PATH)
		var left_hand_node : Spatial = mundo.get_node(LEFT_HAND_PATH)
		if right_hand_node.has_node('Ponta_Indicador_Aponta'):
			right_hand_node = right_hand_node.get_node('Ponta_Indicador_Aponta')
		if left_hand_node.has_node('Ponta_Indicador_Aponta'):
			left_hand_node = left_hand_node.get_node('Ponta_Indicador_Aponta')

		var laser_esquerda : Spatial = cria_laser_invisivel()
		var laser_direita : Spatial = cria_laser_invisivel()
		
		right_hand_node.add_child(laser_direita)
		left_hand_node.add_child(laser_esquerda)

		no_painel_opcoes.raycast1 = laser_direita
		no_painel_opcoes.raycast2 = laser_esquerda
		
		yield(no_painel_opcoes, 'fechou')
		no_painel_opcoes.queue_free()
		laser_direita.queue_free()
		laser_esquerda.queue_free()

	func cria_laser_invisivel() -> Spatial:
		var raycast = RayCast.new()
		raycast.enabled = true
		raycast.collision_mask = 1 << 11
		raycast.cast_to = Vector3(0,0,-.5)
		return raycast

	static func check(line):
		return line.to_lower().begins_with('aguarda marcar x ')
