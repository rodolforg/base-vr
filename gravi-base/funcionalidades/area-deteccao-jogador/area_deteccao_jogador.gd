extends Area

signal jogador_entrou
signal jogador_saiu
signal mao_entrou
signal mao_saiu

# Dimensões do cilindro de detecção do jogador: diâmetro (x) e altura (y)
export(Vector2) var tamanho : Vector2 = Vector2(0.5, 1) setget _set_tamanho
# Se a altura da área de detecção deve ser a partir da origem ou centrada na origem
export(bool) var centrada_na_origem : bool = false setget _set_centrada_na_origem
# Opcional: se filtra grupo de objetos a serem detectados
export(String) var filtro_grupo : String = ''

func _on_AreaDeteccaoJogador_body_entered(body : PhysicsBody):
	if filtro_grupo and not body.is_in_group(filtro_grupo):
		return
	var parent : Node = body
	while parent:
		if parent is ARVROrigin:
			call_deferred('emit_signal', 'jogador_entrou')
			return
		elif parent is ARVRController:
			call_deferred('emit_signal', 'mao_entrou', parent)
			return
		parent = parent.get_parent()

func _on_AreaDeteccaoJogador_body_exited(body : PhysicsBody):
	if filtro_grupo and not body.is_in_group(filtro_grupo):
		return
	var parent : Node = body
	while parent:
		if parent is ARVROrigin:
			call_deferred('emit_signal', 'jogador_saiu')
			return
		elif parent is ARVRController:
			call_deferred('emit_signal', 'mao_saiu', parent)
			return
		parent = parent.get_parent()

func _set_tamanho(p_tamanho: Vector2):
	tamanho = p_tamanho
	var shape : CylinderShape = $CollisionShape.shape
	shape.height = p_tamanho.y
	shape.radius = p_tamanho.x / 2

func _set_centrada_na_origem(p_centrada : bool):
	centrada_na_origem = p_centrada
	
	var col_shape = $CollisionShape
	if p_centrada:
		col_shape.transform.origin = Vector3()
	else:
		col_shape.transform.origin = Vector3(0,tamanho.y/2,0)
