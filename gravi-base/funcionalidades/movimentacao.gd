# Movimentação por analógicos dos controles ARVR
#
# Este nó deve ser filho de nó do tipo:
# 1. ou ARVRController, e isto limita este tipo de movimentação a este controle
# 2. ou ARVRCamera, e isto faz com que ambas as mãos controlem por este movimento
# 3. ou ARVROrigin, e isto também faz com que ambas as mãos controlem por este movimento
#
# Nos casos 2 e 3, ambos fazem o movimento siga apenas o analógico que estiver com
# maior deslocamento no momento da leitura das entradas. Isso significa que não são
# somadas as influências dos valores de ambas as mãos, mas uma é escolhida por "quadro".
# Isso não ocorre caso, por ventura, se coloque um nó deste tipo em cada mão. Nesse caso,
# o movimento resultante acaba sendo a soma vetorial dos analógicos.

extends KinematicBody

export(NodePath) var originNodePath : NodePath
export(NodePath) var cameraNodePath : NodePath
export(float) var maximumSpeed : float = 0.8
export(float) var deadZone : float = 0.15

export(bool) var enableCollision : bool = true setget set_enable_collision
export(bool) var ignore_gravity : bool = false

var playerNode : ARVRCamera
var originNode : ARVROrigin

var controllers : Array = []

const UP = Vector3(0,1,0)
const GRAVITY_ACCEL = Vector3(0,-9.8,0)
var velocity : Vector3 = Vector3(0,0,0)

onready var original_collision_layer = collision_layer

func _ready():
	var parent = get_parent()

	# o jogador (ARVRCamera)
	if cameraNodePath:
		playerNode = get_node(cameraNodePath)
	else:
		if parent is ARVROrigin:
			playerNode = parent.get_node('ARVRCamera')
		elif parent is ARVRController:
			playerNode = parent.get_parent().get_node('ARVRCamera')
		if not (playerNode is ARVRCamera):
			playerNode = null

	# a origem do plano VR (ARVROrigin)
	if originNodePath:
		originNode = get_node(originNodePath)
	else:
		if parent is ARVROrigin:
			originNode = parent
		elif parent is ARVRController:
			originNode = parent.get_parent()
		if not (originNode is ARVROrigin):
			originNode = null

	# Controles
	controllers = _fetch_arvr_controllers(parent)
	

func _physics_process(deltaT):
	var movement_intent : Vector3 = Vector3()
	
	var current_controllers : Array = []
	for c in controllers:
		if c.get_hand() != 0:
			current_controllers.push_back(c)
	
	# Vê qual analógico provoca maior intenção de movimento
	for c in current_controllers:
		var joy_index : int = c.get_joystick_id()
		var new_intent : Vector3 = Vector3()
		new_intent.z = Input.get_joy_axis(joy_index, JOY_ANALOG_LY)
		new_intent.x = Input.get_joy_axis(joy_index, JOY_ANALOG_LX)

		if abs(new_intent.z) < deadZone:
			new_intent.z = 0
		if abs(new_intent.x) < deadZone:
			new_intent.x = 0

		if new_intent.length() > movement_intent.length():
			movement_intent = new_intent

	# Direção do olhar
	var camera_transf = playerNode.get_global_transform()
	var direction = camera_transf.basis[2]
	# Ignora direção vertical
	direction.y = 0
	direction = direction.normalized()

	var speed_vector = (maximumSpeed * -movement_intent.z) * direction * ARVRServer.world_scale
	var x_axis = UP.cross(direction)
	speed_vector += (maximumSpeed * movement_intent.x) * x_axis
	if is_nan(speed_vector.x) or is_nan(speed_vector.y) or is_nan(speed_vector.z):
		return

	if not enableCollision:
		originNode.global_transform.origin += speed_vector * deltaT
		return

	# Place fake body in player's position
	var current_origin = playerNode.global_transform.origin
	current_origin.y = originNode.global_transform.origin.y

	# Change fake body height to match Player's head
	var playerHeight = playerNode.transform.origin.y

	var owner_id = shape_find_owner ( 0 )
	var shape = shape_owner_get_shape ( owner_id, 0 )
	if shape is BoxShape:
		shape.extents.y = playerHeight / 2
	elif shape is CapsuleShape:
		shape.height = playerHeight - shape.radius*2
	var shape_transform = shape_owner_get_transform(owner_id)
	shape_transform.origin.y = playerHeight / 2
	shape_owner_set_transform(owner_id, shape_transform)

	# Try to move fake body
	global_transform.origin = current_origin

	# Efeito da gravidade
	if not ignore_gravity:
		velocity += GRAVITY_ACCEL * deltaT
	
	# Não acumula velocidade no plano
	velocity.x = speed_vector.x
	velocity.z = speed_vector.z

	# Movimenta
	velocity = move_and_slide(velocity, UP)

	# Se está no piso (e não caindo ou pulando), anula velocidade em Y
	if is_on_floor():
		velocity = Vector3()

	# Move ARVROrigin as fake body moved
	var movement = global_transform.origin - current_origin
	originNode.global_transform.origin += movement
	global_transform.origin = current_origin

func set_enable_collision(enable):
	if enable:
		if original_collision_layer:
			$CollisionShape.disabled = false
			shape_owner_set_disabled(0, false)
	else:
		$CollisionShape.disabled = true
		shape_owner_set_disabled(0, true)
	enableCollision = enable

# Busca a partir de nó parent e pode filtrar para buscar apenas por
# controller_id específico. 1 significa mão esquerda, 2 direita, o resto é indefinido.
func _fetch_arvr_controllers(parent : Node, controller_id : int = 0) -> Array:
	var controllers_found : Array = []
	while parent and (not parent is ARVROrigin) and (not parent is ARVRController):
		parent = parent.get_parent()
	if parent is ARVROrigin:
		for child in parent.get_children():
			if child is ARVRController:
				if controller_id == 0 or child.controller_id == controller_id:
					controllers_found.push_back(child)
	elif parent is ARVRController:
		if controller_id == 0 or parent.controller_id == controller_id:
			controllers_found.push_back(parent)
	return controllers_found
