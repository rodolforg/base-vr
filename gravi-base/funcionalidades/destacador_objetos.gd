extends Node
class_name DestacadorObjeto

const GlowingMaterial = preload("res://gravi-base/funcionalidades/glowing.material")

var material_destaque : Material = GlowingMaterial

var lista_objetos = {}

# ou 0.005
var espessura_contorno : float = 0.003

class DadosContorno:
	var contorno : MeshInstance
	var material : Material
	var visivel : bool

	func restaurar():
		contorno.visible = visivel
		contorno.set_surface_material(0, material)

func _get_no_contorno(objeto : Spatial) -> Spatial:
	var contorno
	var objeto_base_para_contorno : MeshInstance

	if objeto.name == "contorno":
		contorno = objeto
	elif objeto.has_node("contorno"):
		contorno = objeto.get_node("contorno")
	elif objeto is MeshInstance:
		objeto_base_para_contorno = objeto
	elif objeto.get_parent() is MeshInstance:
		objeto_base_para_contorno = objeto.get_parent()
	else:
		var num_filhos_malha = 0
		for filho in objeto.get_children():
			if filho is MeshInstance and filho.visible:
				objeto_base_para_contorno = filho
				num_filhos_malha += 1
		if num_filhos_malha > 1:
			printerr('Não há nó de contorno definido e há mais de uma malha disponível')

	if not contorno and objeto_base_para_contorno:
		var malha_contorno = objeto_base_para_contorno.mesh.create_outline(espessura_contorno)
		var novo_no_contorno = MeshInstance.new()
		novo_no_contorno.name = "contorno"
		novo_no_contorno.mesh = malha_contorno
		novo_no_contorno.transform = objeto_base_para_contorno.transform
		novo_no_contorno.visible = false
		objeto.add_child(novo_no_contorno)
		contorno = novo_no_contorno

	return contorno

func destacar(objeto : Spatial) -> bool :
	var lista_contornos : Array = []
	var lista_nos = []

	var contorno = _get_no_contorno(objeto)
	if not contorno:
		return false

	if contorno is MeshInstance:
		lista_nos.append(contorno)
	else:
		for item in contorno.get_children():
			if item is MeshInstance:
				lista_nos.append(item)

	for item in lista_nos:
		var dados : DadosContorno = DadosContorno.new()
		dados.contorno = item
		dados.visivel = item.visible
		dados.material = item.get_surface_material(0)
		lista_contornos.append(dados)

	for item in lista_contornos:
		var material = material_destaque
		if contorno is MeshInstance:
			var material_referencia = contorno.get_surface_material(0)
			if material_referencia and material_referencia is SpatialMaterial:
				material = material_destaque.duplicate(true)
				material.set_shader_param("emission_color", material_referencia.albedo_color)
		if contorno is Spatial:
			contorno.visible = true
		item.contorno.set_surface_material(0, null)
		item.contorno.set_surface_material(0, material)
		item.contorno.visible = true

	lista_objetos[objeto] = lista_contornos

	return true

func retirar_destaque(objeto : Spatial) -> void:
	if not objeto in lista_objetos:
		printerr("Objeto não encontrado na lista de objetos em destaque. Será que conseguiu não criar um contorno de destaque?")
		return
	var lista_contornos = lista_objetos[objeto]
	for contorno in lista_contornos:
		contorno.restaurar()
