# O Laser indicador é um laser feito para selecionar objetos.
#
# Quando o usuário aponta o laser para um objeto que pode ser
# alvo (pertence ao grupo_alvo do laser), ele troca a cor do feixe
# e apresenta um contorno brilhante em torno do objeto alvejado.
#
# Ao pressionar o gatilho do controle de mão VR com um objeto no alvo,
# é emitido um sinal "confirmou_alvo(alvo_confirmado)'.
# O alvo será desselecionado ao se apertar o gatilho novamente.

extends "res://gravi-base/funcionalidades/laser/laser.gd"

signal confirmou_alvo(alvo_confirmado)
signal cancelou_alvo

var alvo_confirmado : Spatial

var objeto_em_destaque
onready var destacador = DestacadorObjeto.new()

export(Color, RGBA) var emissao = Color("ffe300")
export(Color, RGBA) var emissao_colidindo = Color("f365d427")

export(Color, RGBA) var albedo_raio = Color("ffe300")
export(Color, RGBA) var albedo_raio1 = Color("48ffe300")
export(Color, RGBA) var albedo_raio2 = Color("19ffe300")
export(Color, RGBA) var albedo_alvo_raio = Color("ffe300")

export(Color, RGBA) var albedo_raio_colidindo = Color("f365d427")
export(Color, RGBA) var albedo_raio1_colidindo = Color("4835d427")
export(Color, RGBA) var albedo_raio2_colidindo = Color("1914ff00")
export(Color, RGBA) var albedo_alvo_raio_colidindo = Color("f365d427")

func _ready():
	troca_cores(false)

func _input(event):
	var controle = get_parent()
	if event.device != controle.get_joystick_id():
		return
	if event is InputEventJoypadButton and event.is_pressed():
		if event.button_index == JOY_BUTTON_15: # Gatilho frontal # 7: apertar botão A/X
			if alvo_confirmado:
				alvo_confirmado = null
				emit_signal("cancelou_alvo")
			elif alvo_atual:
				alvo_confirmado = alvo_atual
				emit_signal("confirmou_alvo", alvo_confirmado)

func reagir_ao_alvejar():
	troca_cores(true)
	
	$Timer.stop()
	
	if objeto_em_destaque:
		_cancelar_exibicao_contorno()

	var destacou = destacador.destacar(alvo_atual)
	if destacou:
		objeto_em_destaque = alvo_atual

func reagir_ao_nao_alvejar():
	troca_cores(false)
	
	if objeto_em_destaque:
		get_node("Timer").start()

func define_cores(cor_raio : Color, cor_raio1 : Color, cor_raio2 : Color, cor_alvo_raio : Color, cor_emissao : Color) -> void:
	no_raio.mesh.surface_get_material(0).set_shader_param('albedo', cor_raio)
	no_raio_externo_1.mesh.surface_get_material(0).set_shader_param('albedo', cor_raio1)
	no_raio_externo_2.mesh.surface_get_material(0).set_shader_param('albedo', cor_raio2)
	no_indicador_alvo.mesh.surface_get_material(0).set_shader_param('albedo', cor_alvo_raio)
	
	no_raio.mesh.surface_get_material(0).set_shader_param('emission', cor_emissao)
	no_raio_externo_1.mesh.surface_get_material(0).set_shader_param('emission', cor_emissao)
	no_raio_externo_2.mesh.surface_get_material(0).set_shader_param('emission', cor_emissao)
	no_indicador_alvo.mesh.surface_get_material(0).set_shader_param('emission', cor_emissao)

func troca_cores(colidindo : bool) -> void:
	if colidindo:
		define_cores(albedo_raio_colidindo, albedo_raio1_colidindo, albedo_raio2_colidindo, albedo_alvo_raio_colidindo, emissao_colidindo)
	else:
		define_cores(albedo_raio, albedo_raio1, albedo_raio2, albedo_alvo_raio, emissao)


func _on_Timer_timeout():
	_cancelar_exibicao_contorno()

func _cancelar_exibicao_contorno() -> void:
	destacador.retirar_destaque(objeto_em_destaque)

func habilitar(habilitado : bool) -> void:
	print_debug("habilitar laser indicador ", habilitado)
	enabled = habilitado
	visible = habilitado
	set_process_input(habilitado)
	
func on_visibilidade_mudou():
	pass
