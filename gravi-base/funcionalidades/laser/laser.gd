# Exibe um raio laser.
#
# O laser é exibido a partir do nó 3D/espacial no_referencia, orientado na direção -Z,
# deslocado de distancia_inicial.
# Este raio laser não acompanha mudanças na posição e orientação do no_referencia
# por padrão. A ideia é que este nó de raio seja filho daquele que movimentará ou
# apontará o raio, como o controle de mão VR.
#
# Quando acontece uma colisão do raio com algum objeto:
# - exibe uma esfera no local de contato (nó "indicador_alvo")
# - executa o método reagir_ao_alvejar()
# - emite o sinal "entrou_na_mira(objeto)"
# - atualiza as propriedades:
#    - "alvo_atual", contendo o nó de colisão; e
#    - "ponto_colisao", indicando a posição da colisão em coordenadas globais
# Ao deixar de colidir com algum objeto:
# - executa o método reagir_ao_nao_alvejar()
# - emite o sinal "saiu_da_mira(objeto)"
# - limpa as propriedades "alvo_atual" e "ponto_colisao"
#
# O raio colide com outros objetos conforme seu Collision Mask. Há a possibilidade
# de filtrar as colisões também apenas para aqueles que pertençam ao grupo definido
# na propriedade "grupo_alvo", caso ela não esteja em branco.
# 
# Este raio obrigatoriamente é desabilitado quando fica invisível e reabilitado
# quando visível.

extends RayCast

signal entrou_na_mira(objeto)
signal saiu_da_mira(objeto)

# Nó que serve para referência de posição para o Laser em relação ao controle de mão VR
export(NodePath) var no_referencia : NodePath = "../Ponta_Indicador_Aponta"
# Uma distância a mais em relação ao nó de referência para iniciar a aparência do Laser
export(float) onready var distancia_inicial : float = 0.0 setget _set_distancia_inicial;

export(String) var grupo_alvo : String

var alvo_atual : Object
var ponto_colisao : Vector3

onready var no_indicador_alvo : MeshInstance = get_node("indicador_alvo")
onready var no_raio : MeshInstance = get_node("raio")
onready var no_raio_externo_1 : MeshInstance = get_node("raio externo 1")
onready var no_raio_externo_2 : MeshInstance = get_node("raio externo 2")

onready var COMPRIMENTO_MAX_RAIO : float = 0

func _ready():
	COMPRIMENTO_MAX_RAIO = no_raio.mesh.height
	
	var ok = connect("visibility_changed", self, "on_visibilidade_mudou")
	if ok != OK:
		printerr("(Laser) Não conseguiu conectar ao sinal 'visibility_changed'")
	on_visibilidade_mudou()

	var referencia : Spatial = get_node(no_referencia)
	if referencia:
		global_transform = referencia.global_transform
		global_rotate(referencia.global_transform.basis[0], deg2rad(90))

func _physics_process(_delta):
	var alvo_anterior = alvo_atual
	
	var raycast = self
	if raycast.is_colliding():
		ajustar_indicador_alvo()
		ajustar_comprimento_raio()
		if (not grupo_alvo) or raycast.get_collider().is_in_group(grupo_alvo):
			alvo_atual = raycast.get_collider()
			ponto_colisao = raycast.get_collision_point()
		else:
			alvo_atual = null
			ponto_colisao = Vector3()
	else:
		no_indicador_alvo.visible = false
		alvo_atual = null
		ponto_colisao = Vector3()
		no_raio.mesh.surface_get_material(0).set_shader_param('comprimento', COMPRIMENTO_MAX_RAIO)
		no_raio_externo_1.mesh.surface_get_material(0).set_shader_param('comprimento', COMPRIMENTO_MAX_RAIO)
		no_raio_externo_2.mesh.surface_get_material(0).set_shader_param('comprimento', COMPRIMENTO_MAX_RAIO)
		
	if alvo_atual != alvo_anterior:
		if alvo_anterior:
			emit_signal("saiu_da_mira", alvo_anterior)
		if alvo_atual:
			reagir_ao_alvejar()
			emit_signal("entrou_na_mira", alvo_atual)
		else:
			reagir_ao_nao_alvejar()

func on_visibilidade_mudou() :
	enabled = visible

func ajustar_indicador_alvo():
	no_indicador_alvo.visible = true
	no_indicador_alvo.global_transform.origin = get_collision_point()

	no_indicador_alvo.transform.basis = Basis()

	var escala = lerp(.8, 2, min(1, no_indicador_alvo.transform.origin.y / -3))
	no_indicador_alvo.transform.basis = no_indicador_alvo.transform.basis.scaled(Vector3(escala,1,escala))

func ajustar_comprimento_raio():
	var novo_comprimento : float = - global_transform.xform_inv(get_collision_point()).y
	no_raio.mesh.surface_get_material(0).set_shader_param('comprimento', novo_comprimento)
	no_raio_externo_1.mesh.surface_get_material(0).set_shader_param('comprimento', novo_comprimento)
	no_raio_externo_2.mesh.surface_get_material(0).set_shader_param('comprimento', novo_comprimento)

func _set_distancia_inicial(nova_distancia : float):
	if nova_distancia < 0:
		nova_distancia = 0
	if not no_raio:
		call_deferred('_set_distancia_inicial', nova_distancia)
		return
	distancia_inicial = nova_distancia
	no_raio.mesh.surface_get_material(0).set_shader_param('distancia_inicial', nova_distancia)
	no_raio_externo_1.mesh.surface_get_material(0).set_shader_param('distancia_inicial', nova_distancia)
	no_raio_externo_2.mesh.surface_get_material(0).set_shader_param('distancia_inicial', nova_distancia)

func reagir_ao_alvejar():
	pass

func reagir_ao_nao_alvejar():
	pass
