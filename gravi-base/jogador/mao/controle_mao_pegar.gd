extends Spatial

var objeto_na_mao = null
var modo_objeto_na_mao = null
var colidiu_com_mao_aberta = false

const DEVE_TROCAR_PAI_OBJETO_NA_MAO = false
var pai_objeto_na_mao = null

var velocidades = []
var posicoes = []
const QTD_RASTREAMENTO = 20
var velocidade_media = Vector3()

onready var mao : Spatial = get_parent()

var objeto_proximo : PhysicsBody
onready var _material_contorno : Material = preload("res://gravi-base/funcionalidades/glowing.material").duplicate()
var destacador : DestacadorObjeto

export(bool) var so_destacaveis = false
export(bool) var so_marcados = false

onready var _no_referencia_objeto : Spatial = $ReferenciaObjeto

func _ready() -> void:
	mao.add_user_signal("pegou", [{"objeto":TYPE_OBJECT}])
	mao.add_user_signal("soltou", [{"objeto":TYPE_OBJECT}])
	_material_contorno.set_shader_param("emission_color", Color("#c8c115"))
	_material_contorno.set_shader_param("period", 0.8)
	destacador = DestacadorObjeto.new()
	destacador.material_destaque = _material_contorno

func _process(delta) -> void:
	if esta_a_mao_aberta():
		largar_objeto(true)
		return
	
	if objeto_na_mao and not DEVE_TROCAR_PAI_OBJETO_NA_MAO:
		mover_objeto_na_mao()
	
	if objeto_na_mao:
		rastrear_movimento(delta)

func _input(event) -> void:
	if event is mao.HandEventAction:
		if event.hand == mao.nome_mao:
			if event.is_action_pressed('punho'):
				if objeto_proximo:
					pegar_objeto(objeto_proximo)
					colidiu_com_mao_aberta = false

func is_objeto_pegavel(objeto : PhysicsBody) -> bool:
	if so_marcados:
		if 'pegavel' in objeto:
			return objeto.pegavel
		if objeto.has_method("pegar"):
			return true
		else:
			return false
	return objeto is RigidBody # and objeto.has_method("pegar")

func pegar_objeto(objeto) -> void:
	if not objeto:
		return

	objeto_na_mao = objeto

	parar_destaque_objeto()
	
	if objeto_na_mao is RigidBody:
		modo_objeto_na_mao = objeto.mode
		if not objeto.has_meta('modo_anterior'):
			objeto.set_meta('modo_anterior', objeto.mode)
		objeto_na_mao.mode = RigidBody.MODE_STATIC

	if DEVE_TROCAR_PAI_OBJETO_NA_MAO:
		pai_objeto_na_mao = objeto.get_parent()
		trocar_no_pai(objeto, self)
		objeto.transform.origin = Vector3(0,0,0)
	
	if objeto_na_mao.has_method('pegar'):
		objeto_na_mao.pegar(self)
	
	mao.emit_signal("pegou", objeto_na_mao)
	
	posicoes.clear()
	posicoes.append(global_transform.origin)
	velocidades.clear()
	velocidade_media = Vector3()

func mover_objeto_na_mao() -> void:
	objeto_na_mao.global_transform = _no_referencia_objeto.global_transform.orthonormalized()

func largar_objeto(mantem_inercia) -> void:
	if objeto_na_mao:
		if objeto_na_mao is RigidBody:
			if objeto_na_mao.has_meta('modo_anterior'):
				objeto_na_mao.mode = objeto_na_mao.get_meta('modo_anterior')
			else:
				objeto_na_mao.mode = modo_objeto_na_mao
		if DEVE_TROCAR_PAI_OBJETO_NA_MAO:
			var current_global_transform = objeto_na_mao.global_transform
			trocar_no_pai(objeto_na_mao, pai_objeto_na_mao)
			objeto_na_mao.global_transform = current_global_transform
		if objeto_na_mao.has_method('soltar'):
			objeto_na_mao.soltar(self)
		if mantem_inercia:
			objeto_na_mao.apply_impulse(Vector3.ZERO, velocidade_media)
		mao.emit_signal("soltou", objeto_na_mao)
	objeto_na_mao = null
	pai_objeto_na_mao = null


func _on_controle_pegar_body_entered(body) -> void:
	if esta_a_mao_aberta():
		colidiu_com_mao_aberta = true
	if not objeto_proximo and not objeto_na_mao and is_objeto_pegavel(body):
		destacar_objeto(body)

func _on_controle_pegar_body_exited(body) -> void:
	colidiu_com_mao_aberta = false
	if objeto_proximo == body:
		parar_destaque_objeto()

func destacar_objeto(body) -> void:
	var destacou = destacador.destacar(body)
	if destacou or not so_destacaveis:
		objeto_proximo = body

func parar_destaque_objeto() -> void:
	if not objeto_proximo:
		return

	destacador.retirar_destaque(objeto_proximo)
	objeto_proximo = null

func esta_a_mao_fechada() -> bool:
	return mao.polegar_pressionado and mao.indicador_pressionado and mao.resto_pressionado
	
func esta_a_mao_aberta() -> bool:
	return mao.pose_atual != mao.Poses.PUNHO and mao.pose_atual != mao.Poses.JOINHA

func trocar_no_pai(obj, novo_pai) -> void:
	var pai_anterior = obj.get_parent()
	pai_anterior.remove_child(obj)
	novo_pai.add_child(obj)
	
func rastrear_movimento(delta) -> void:
	if posicoes.size() >= QTD_RASTREAMENTO:
		posicoes.pop_front()
		velocidades.pop_front()
	
	var ultima_posicao = posicoes.back()
	posicoes.append(global_transform.origin)
	velocidades.append((posicoes.back() - ultima_posicao)/delta)

	var v_soma = Vector3()
	for v in velocidades:
		v_soma += v
	velocidade_media = v_soma / velocidades.size()
