# Controla uma mão em através de controle VR Oculus Touch
# - Anima automaticamente e de maneira simples: posição do polegar, do indicador e dos demais dedos
# - Ativa e desativa as formas de colisão da palma da mão (grosseira)
# - Adiciona algumas propriedades:
#   - nome_mao: identifica a mão: "esquerda" ou "direita" (somente leitura)
#   - pose_atual: identifica a pose atual: MAO_ABERTA, PUNHO (fechado), MAO_APONTA, JOINHA ou OUTRO (somente leitura)
#   - objeto_segurado: contém o nó do objeto segurado pela mão (leitura e escrita)
# - Cria automaticamente entradas no Mapa de Entradas (Input Map) e gera seus eventos:
#   - menu_jogo: botão de menu na mão esquerda
#   - nome da ação completa, que é nome da ação + '_' + nome da mão:
#        Exemplos: polegar_esquerda (tocou o botão A/X do controle ou não),
#                  indicador-aponta_direita (não tocou o botão gatilho/trigger do controle ou tocoi),
#                  indicador-encosta_esquerda (tocou o botão gatilho/trigger do controle ou não),
#                  indicador-apertado_esquerda (apertou o botão gatilho/trigger do controle ou não),
#                  resto_direita (apertou o botão agarro/grab/grip ou não),
# - Gera eventos de entrada de tipo próprio, HandEventAction, que informam mudança da pose da mão:
#   - pressed: se foi ativada ou não
#   - device: o joystick_id do controle
#   - hand: o nome do "lado" da mão: "esquerda" ou "direita"
#   - action: o nome da pose (atual, se pressed for verdadeiro ou a anterior, se falso)
# - Emite sinais:
#   - "controle_ativado", sempre que o controle é ativado pelo rastreador VR
#   - "pegou", quando a mão pega um objeto
#   - "soltou", quando a mão solto um objeto

extends ARVRController

signal controle_ativado(controller)

onready var ws = ARVRServer.world_scale

var nome_mao

onready var _controle_pegar : Node = $controle_pegar
var objeto_segurado setget _set_objeto_segurado, _get_objeto_segurado

var polegar_pressionado = false
var resto_pressionado = false
var indicador_aponta_pressionado = false
var indicador_encosta_pressionado = false
var indicador_apertado_pressionado = false

enum Poses {MAO_ABERTA, PUNHO, MAO_APONTA, JOINHA, OUTRO}

export(Poses) var pose_atual = Poses.OUTRO

onready var _no_ponta_indicador : Position3D = $Ponta_Indicador
onready var _no_ponta_indicador_aponta : Position3D = $Ponta_Indicador_Aponta
onready var _no_ponta_indicador_repouso : Position3D = $Ponta_Indicador_Repouso

func nome_pose(pose) -> String:
	match(pose):
		Poses.MAO_ABERTA:
			return "mao_aberta"
		Poses.PUNHO:
			return "punho"
		Poses.MAO_APONTA:
			return "mao_aponta"
		Poses.JOINHA:
			return "joinha"
		#Poses.OUTRO:
		_:
			return "outro"

func nome_pose_atual() -> String:
	return nome_pose(pose_atual)

func _ready():
	visible = false

	var nomes_acoes_dedos = ["polegar_", "indicador-encosta_", "indicador-aponta_", "indicador-apertado_", "resto_"]
	
	for acao_dedo in nomes_acoes_dedos:
		for mao in ['esquerda', 'direita']:
			var acao = acao_dedo + mao
			if not InputMap.has_action(acao):
				InputMap.add_action(acao)

	if not InputMap.has_action(nome_pose(Poses.PUNHO)):
		for pose in range(Poses.size()):
			if pose != Poses.OUTRO:
				InputMap.add_action(nome_pose(pose))
	
	if not InputMap.has_action('menu_jogo'):
		InputMap.add_action('menu_jogo')

func _process(_delta):
	var new_ws = ARVRServer.world_scale
	if (ws != new_ws):
		ws = new_ws
		$controller_mesh.scale = Vector3(ws, ws, ws)
		
	if !get_is_active():
		visible = false
	elif !visible:
		visible = true
		
		if get_hand() == ARVRPositionalTracker.TRACKER_LEFT_HAND:
			nome_mao = "esquerda"
		elif get_hand() == ARVRPositionalTracker.TRACKER_RIGHT_HAND:
			nome_mao = "direita"

		emit_signal("controle_ativado", self)

func _input(event):
	if not nome_mao:
		return
	if event.device != get_joystick_id():
		return
	if event is InputEventJoypadButton:
		var action_name : String
		match event.button_index:
			JOY_BUTTON_5: # toque no botão A
				action_name = "polegar_" + nome_mao
				polegar_pressionado = event.pressed
			JOY_BUTTON_11: # gatilho da frente encostado
				action_name = "indicador-encosta_" + nome_mao
				indicador_encosta_pressionado = event.pressed
			JOY_BUTTON_12: # gatilho da frente solto
				action_name = "indicador-aponta_" + nome_mao
				indicador_aponta_pressionado = event.pressed
			JOY_BUTTON_15: # gatilho da frente
				action_name = "indicador-apertado_" + nome_mao
				indicador_apertado_pressionado = event.pressed
			JOY_BUTTON_2: # gatilho lateral
				action_name = "resto_" + nome_mao
				resto_pressionado = event.pressed
		
		if not action_name:
			if event.button_index == JOY_BUTTON_3 and nome_mao == 'esquerda':
				action_name = 'menu_jogo'
			
		if action_name:
			if event.pressed:
				Input.action_press(action_name)
			else:
				Input.action_release(action_name)
		
		if action_name == "indicador-apertado_" + nome_mao:
			if event.pressed:
				$KinematicBody/IndicadorShape.disabled = true
				$KinematicBody/Indicador_ApontaShape.disabled = true
			else:
				yield(get_tree().create_timer(.5),"timeout")
				if not indicador_aponta_pressionado:
					$KinematicBody/IndicadorShape.disabled = false
					$KinematicBody/Indicador_ApontaShape.disabled = true
				else:
					$KinematicBody/IndicadorShape.disabled = true
					$KinematicBody/Indicador_ApontaShape.disabled = false
		
		if action_name.begins_with('indicador-aponta'):
			_no_ponta_indicador.transform = _no_ponta_indicador_aponta.transform
		elif action_name.begins_with('indicador-encosta'):
			_no_ponta_indicador.transform = _no_ponta_indicador_repouso.transform
			
		if action_name == "resto_" + nome_mao:
			if event.pressed:
				$KinematicBody/MedioShape.disabled = true
				$KinematicBody/AnelarShape.disabled = true
				$KinematicBody/MinimoShape.disabled = true
			else:
				yield(get_tree().create_timer(.5),"timeout")
				if not resto_pressionado:
					$KinematicBody/MedioShape.disabled = false
					$KinematicBody/AnelarShape.disabled = false
					$KinematicBody/MinimoShape.disabled = false

func _physics_process(_delta):
	var nova_pose
	if not nome_mao:
		return
	
	if Input.is_action_pressed("polegar_" + nome_mao) and Input.is_action_pressed("indicador-apertado_" + nome_mao) and Input.is_action_pressed("resto_" + nome_mao):
		nova_pose = Poses.PUNHO

	elif Input.is_action_pressed("polegar_" + nome_mao) and Input.is_action_pressed("indicador-aponta_" + nome_mao) and Input.is_action_pressed("resto_" + nome_mao):
		nova_pose = Poses.MAO_APONTA

	elif not Input.is_action_pressed("polegar_" + nome_mao) and Input.is_action_pressed("indicador-apertado_" + nome_mao) and Input.is_action_pressed("resto_" + nome_mao):
		nova_pose = Poses.JOINHA

	elif not Input.is_action_pressed("polegar_" + nome_mao) and ((Input.is_action_pressed("indicador-aponta_" + nome_mao)) or (Input.is_action_pressed("indicador-encosta_" + nome_mao))) and not (Input.is_action_pressed("resto_" + nome_mao)):
		nova_pose = Poses.MAO_ABERTA

	else:
		nova_pose = Poses.OUTRO
		
	if nova_pose != pose_atual:
		if pose_atual != Poses.OUTRO:
			var acao = HandEventAction.new(self, nome_pose(pose_atual), false)
			Input.parse_input_event(acao)
		var acao = HandEventAction.new(self, nome_pose(nova_pose), true)
		Input.parse_input_event(acao)
		pose_atual = nova_pose
	
	anima_mao()

func anima_mao() -> void:
	if not nome_mao:
		return
	if Input.is_action_just_pressed("indicador-aponta_" + nome_mao):
		$"controle_animação/Indicador".play_backwards("de_aponta_para_repouso")
	if Input.is_action_just_pressed("indicador-encosta_" + nome_mao):
		$"controle_animação/Indicador".play("de_aponta_para_repouso")
	if Input.is_action_just_pressed("indicador-apertado_" + nome_mao):
		$"controle_animação/Indicador".play("de_repouso_para_aperto")
	if Input.is_action_just_pressed("polegar_" + nome_mao):
		$"controle_animação/Polegar".play("vai")
	if Input.is_action_just_pressed("resto_" + nome_mao):
		$"controle_animação/Mão".play("vai")
	if Input.is_action_just_released("indicador-apertado_" + nome_mao):
		$"controle_animação/Indicador".play_backwards("de_repouso_para_aperto")
	if Input.is_action_just_released("polegar_" + nome_mao):
		$"controle_animação/Polegar".play_backwards("vai")
	if Input.is_action_just_released("resto_" + nome_mao):
		$"controle_animação/Mão".play_backwards("vai")

func _get_objeto_segurado() -> Node :
	if not _controle_pegar:
		return null
	return _controle_pegar.objeto_na_mao

func _set_objeto_segurado(objeto) -> void:
	if not _controle_pegar:
		print_debug('Não há nó de controle para pegar objetos')
		objeto_segurado = objeto
		return
	if not objeto or _controle_pegar.objeto_na_mao:
		_controle_pegar.largar_objeto(false)
	_controle_pegar.pegar_objeto(objeto)
	objeto_segurado = objeto

class HandEventAction:
	extends InputEventAction
	
	var hand
	
	func _init(controller: ARVRController, action : String, pressed : bool):
		self.pressed = pressed
		self.device = controller.get_joystick_id()
		self.action = action
		hand = controller.nome_mao
