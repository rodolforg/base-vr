# Configura o Viewport para RV 
# A Camera VR está configurada para não exibir a camada visual #3
# para que o objeto Oculus, que está nessa camada, não seja visível em VR,
# só pelo PC

extends ARVROrigin

func _ready():
	setup_vr_interface()

func setup_vr_interface():
	var interfaces = find_viable_interfaces()
	for interface in interfaces:
		if interface.initialize():
			setup_viewport()
			setup_engine()
			break
	var interface = ARVRServer.get_primary_interface()
	if not interface:
		printerr('Nenhum HMD de RV disponível')
		var ok = get_tree().create_timer(1).connect("timeout", self, "setup_vr_interface")
		if ok != OK:
			printerr("Não é possível verificar a presença do HMD de RV outra vez")

func setup_viewport():
	var viewport = get_viewport()

	# force to use VR features
	viewport.arvr = true
	
	if viewport.size == Vector2(0,0):
		if viewport.get_parent() is ViewportContainer:
			var parent = viewport.get_parent()
			viewport.size = parent.rect_size
			parent.connect('resized', self, 'on_container_resized')
		else:
			viewport.size = Vector2(100,100)

func setup_engine():
	# make sure vsync is disabled or we'll be limited to 60fps
	OS.vsync_enabled = false
		
	# up our physics to 90fps to get in sync with our rendering
	Engine.target_fps = 90

func find_viable_interfaces():
	var viable_interfaces = []
	for interface_info in ARVRServer.get_interfaces():
		var interface = ARVRServer.find_interface(interface_info.name)
		var capabilities = interface.get_capabilities()
		if capabilities & ARVRInterface.ARVR_STEREO and capabilities & ARVRInterface.ARVR_EXTERNAL:
			viable_interfaces.push_back(interface)
	return viable_interfaces

func on_container_resized():
	var viewport = get_viewport()
	viewport.size = viewport.get_parent().rect_size
	print('redimensionado: ', ARVRServer.get_primary_interface().get_render_targetsize())
