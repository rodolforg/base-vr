extends Spatial

enum Comportamento {Reduz, Some}

export(NodePath) var caminho_alvo : NodePath setget set_caminho_alvo
export(Comportamento) var comportamento = Comportamento.Reduz
export(float) var limite_1 : float = 1.0
export(float) var limite_2 : float = 0.3

var alvo : Spatial setget set_alvo

func _ready():
	set_physics_process(false)

func set_caminho_alvo(novo_caminho_alvo : NodePath):
	if novo_caminho_alvo == caminho_alvo:
		return

	var no = get_node(novo_caminho_alvo)
	# Caso alvo ainda não tenha sido colocado em cena, aguarda um quadro
	if not no and novo_caminho_alvo:
		call_deferred("set_caminho_alvo", novo_caminho_alvo)
		return

	caminho_alvo = novo_caminho_alvo
	set_alvo(no)

func set_alvo(novo_alvo : Spatial):
	if novo_alvo == alvo:
		return

	alvo = novo_alvo
	caminho_alvo = alvo.get_path() if alvo else NodePath()

	if alvo:
		set_physics_process(true)
	else:
		set_physics_process(false)

func _physics_process(_delta):
	var posicao_alvo : Vector3 = alvo.global_transform.origin
	look_at(posicao_alvo, Vector3(0,1,0))
	var distancia : float = global_transform.origin.distance_to(posicao_alvo)

	match comportamento:
		Comportamento.Reduz:
			var nova_escala : float
			if distancia >= limite_1:
				nova_escala = 1.0
			elif distancia > limite_2:
				nova_escala = distancia/limite_1
			else:
				nova_escala = limite_2/limite_1
			scale = Vector3(nova_escala, nova_escala, nova_escala)
		Comportamento.Some:
			var aparecer = distancia > limite_1
			if $TweenSumir.is_active():
				return
			var tem_animacao = false
			if aparecer and 1.0 > $seta.get_surface_material(0).get_shader_param("albedo").a:
				$TweenSumir.interpolate_method(self, "_alterar_alpha", 0, 1, 0.4,Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
				tem_animacao = true
			elif not aparecer and 0.0 < $seta.get_surface_material(0).get_shader_param("albedo").a:
				$TweenSumir.interpolate_method(self, "_alterar_alpha", 1, 0, 0.4,Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
				tem_animacao = true
			if tem_animacao:
				$TweenSumir.start()

func _alterar_alpha(a : float):
	var albedo = $seta.get_surface_material(0).get_shader_param("albedo")
	var emission = $seta.get_surface_material(0).get_shader_param("emission")
	albedo.a = a
	emission.a = a
	$seta.get_surface_material(0).set_shader_param("albedo", albedo)
	$seta.get_surface_material(0).set_shader_param("emission", emission)
