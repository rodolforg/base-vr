# Exibe um painel para mostrar o objetivo atual
# O objetivo é um texto
#
# Por padrão, ele pode ser ocultado quando o usuário fechar a mão.
# Isso pode ser alterado pela variável fecha_com_a_mao

extends Spatial

export(String) var texto : String setget set_texto, get_texto
export(bool) var fecha_com_a_mao : bool = true setget _set_visibilidade_fechar_com_a_mao

signal fechou

onready var _label : Label = $"Viewport/Tela Objetivo/Texto"
onready var _anim_player_feche_a_mao : AnimationPlayer = $"Viewport/Tela Objetivo/AnimationPlayer_feche_a_mao"

onready var _texto_feche_a_mao : Label = $"Viewport/Tela Objetivo/feche_a_mao"
onready var _sprite_feche_a_mao : Sprite = $"Viewport/Tela Objetivo/Sprite"

func _ready():
	set_process(visible)
	var ok = connect("visibility_changed", self, "on_visibility_change")
	if ok != OK:
		printerr("(Painel Objetivo) Não conseguiu conectar ao sinal 'visibility_changed'")

func _process(_delta):
	if fecha_com_a_mao and (Input.is_action_just_pressed('punho') or Input.is_action_just_pressed('joinha')):
		visible = false
		emit_signal('fechou')

func set_texto(novo_texto : String):
	novo_texto = novo_texto.replace("\\n", "\n")
	_label.text = novo_texto
	texto = novo_texto

func get_texto() -> String:
	return _label.text

func get_fechavel() -> bool:
	return fecha_com_a_mao

func on_visibility_change():
	set_process(visible)

	if visible:
		_anim_player_feche_a_mao.play('Piscar texto como fechar')

func _set_visibilidade_fechar_com_a_mao(visivel : bool) -> void:
	_sprite_feche_a_mao.visible = visivel
	_texto_feche_a_mao.visible = visivel
