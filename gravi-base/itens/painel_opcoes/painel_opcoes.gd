# Exibe um painel para mostrar um painel de múltiplas opções
#
# Uma vez exibido o painel, o usuário pode marcar uma ou mais opções e,
# para concluir, ele pressiona o botão OK.
#
# As opções são definidas em... "opcoes" (tipo lista/vetor)
# Elas podem ser exibidas na ordem armazenada ou aleatoriamente.
# Isso é definido em "opcoes_em_ordem_aleatoria"
# As opções corretas são em... "opcoes_corretas" (tipo lista/vetor)
#
# Por padrão, não se mostra a correção ao usuário após ele pressionar OK.
# Caso queira exibir, defina "mostrar_correcao_ao_concluir" para verdadeira.
#
# Caso só se permita marcar uma das opções existentes, defina como verdadeira
# a propriedade "so_aceitar_marcar_uma_opcao".
#
# Os seguintes sinais existem:
# - marcou : quando o usuário pressiona OK, indicando que marcou suas escolhas
# - fechou : quando o usuário acabou de usar tudo (marcou as opções e, se permitido,
#              terminou de ver a correção)
#
# As opções marcadas pelo usuário estão em "opcoes_marcadas".
# Também há "qtd_acertos" (número absoluto), "qtd_erros" (número absoluto) e
# "percentual_acerto" (número relativo, de 0,0 a 1,0).
# Essas informações são apagadas ao painel ser reexibido (visible) ou
# ao se alterar as opções ou as opções corretas.

extends Spatial

export(PoolStringArray) var opcoes : PoolStringArray = ['Sim', 'Não'] setget _set_opcoes
export(PoolStringArray) var opcoes_corretas : PoolStringArray = ['Sim'] setget _set_opcoes_corretas
export(bool) var opcoes_em_ordem_aleatoria : bool = false setget _set_opcoes_em_ordem_aleatoria
export(bool) var mostrar_correcao_ao_concluir : bool = true
export(bool) var so_aceitar_marcar_uma_opcao : bool = false

var raycast1 : RayCast
var raycast2 : RayCast

var opcoes_marcadas : PoolStringArray = []
var qtd_acertos : int = 0
var qtd_erros : int = 0
var percentual_acerto : float = 0.0

signal fechou
signal marcou

const BOTAO_GATILHO : int = 15

onready var _container : Container = $"Viewport/tela_opcoes/VBoxContainer2/VBoxContainer"
onready var _instancia : MeshInstance = $objeto/MeshInstance
onready var _ponto_colisao : MeshInstance = $objeto/PontoColisao
onready var _viewport : MeshInstance = $Viewport
onready var _botao_ok : Button = $"Viewport/tela_opcoes/VBoxContainer2/HBoxContainer/botao_ok"

var _estado_anterior_gatilho_esquerdo : bool = false
var _estado_anterior_gatilho_direito : bool = false

var _botao_marcado_anterior : Button = null

var _audio_acerto : AudioStream = preload("res://gravi-base/sons/acertou.wav")
var _audio_erro : AudioStream = preload("res://gravi-base/sons/errou.wav")
var _audio_selecao : AudioStream = preload("res://gravi-base/sons/selecionou.wav")
var _audio_desselecao : AudioStream = preload("res://gravi-base/sons/desselecionou.wav")

var _modo_correcao : bool = false

func _ready():
	set_process(visible)
	var ok = connect("visibility_changed", self, "on_visibility_change")
	if ok != OK:
		printerr("(Painel Opções) Não conseguiu conectar ao sinal 'visibility_changed'")
	
	# atualiza tamanho de viewport se suas telas visíveis mudarem de tamanho
	for tela in _viewport.get_children():
		tela.connect("resized", self, "_recalcular_tamanho_viewport")
		tela.connect("visibility_changed", self, "_recalcular_tamanho_viewport")
	# 3.5
	#ok = connect("child_entered_tree", self, )

func _process(_delta):
	var ponto : Vector3
	
	var raycast_que_colidiu : RayCast = null
	if raycast1 and raycast1.is_colliding() and raycast1.get_collision_normal().z > 0:
		ponto = _instancia.to_local(raycast1.get_collision_point())
		raycast_que_colidiu = raycast1
	elif raycast2 and raycast2.is_colliding() and raycast2.get_collision_normal().z > 0:
		ponto = _instancia.to_local(raycast2.get_collision_point())
		raycast_que_colidiu = raycast2
	
	if not raycast_que_colidiu:
		_estado_anterior_gatilho_direito = false
		_estado_anterior_gatilho_esquerdo = false
	
	if not ponto:
		_ponto_colisao.visible = false
	else:
		_ponto_colisao.visible = true
		_ponto_colisao.transform.origin = ponto
		_ponto_colisao.look_at(raycast_que_colidiu.global_transform.origin, Vector3(0,1,0))
		
		var botao_em_foco : Button = null
		var y = _converter_y_para_viewport(ponto.y)
		if not _modo_correcao:
			for botao in _container.get_children():
				if botao is Button:
					if botao.has_focus():
						botao.release_focus()
					if botao.visible and (not botao.disabled) and _esta_dentro_da_altura(y, botao):
						botao.grab_focus()
						botao_em_foco = botao
						break
		if not botao_em_foco:
			if _esta_dentro_da_altura(y, _botao_ok):
				_botao_ok.grab_focus()
				botao_em_foco = _botao_ok
			else:
				_botao_ok.release_focus()
		
		##### Verifica estado dos gatilhos do controle que está apontando
		var estado_anterior : bool
		
		var controle : ARVRController = _buscar_controle_vr(raycast_que_colidiu)
		if not controle:
			printerr('O raycast que colide com o painel de opções tem que ser filho (direto ou não) de um controle de mão')
			return
		
		if controle.controller_id == 1: # = Esquerdo
			estado_anterior = _estado_anterior_gatilho_esquerdo
		elif controle.controller_id == 2: # Direito
			estado_anterior = _estado_anterior_gatilho_direito
		else:
			printerr('Controle não identificado como esquerdo ou direito. Ignorado')
			return

		var estado_atual = controle.is_button_pressed(BOTAO_GATILHO)
		
		var gatilho_recem_pressionado = (not estado_anterior) and estado_atual
		#var gatilho_recem_liberado = estado_anterior and (not estado_atual)
		
		if controle.controller_id == 1: # = Esquerdo
			_estado_anterior_gatilho_esquerdo = estado_atual
		elif controle.controller_id == 2: # Direito
			_estado_anterior_gatilho_direito = estado_atual

		##### Fim da verificação do estado do gatilho

		if botao_em_foco:
			if gatilho_recem_pressionado:
				if botao_em_foco != _botao_ok:
					if _modo_correcao:
						return
					#botao_em_foco.toggle_mode = true
					botao_em_foco.pressed = not botao_em_foco.pressed
					
					if botao_em_foco.pressed and so_aceitar_marcar_uma_opcao:
						if _botao_marcado_anterior and _botao_marcado_anterior != botao_em_foco:
							_botao_marcado_anterior.pressed = false
					_botao_marcado_anterior = botao_em_foco
					_atualizar_botao_ok()
					# emitir som
					var audio_player : AudioStreamPlayer = $AudioStreamPlayer
					if botao_em_foco.pressed:
						audio_player.stream = _audio_selecao
					else:
						audio_player.stream = _audio_desselecao
					audio_player.play()
				elif not _botao_ok.disabled:
					if not _modo_correcao:
						_computar_resultado()
						# exibir correção
						if mostrar_correcao_ao_concluir:
							_mostrar_correcao()
						# emitir som
						var audio_player : AudioStreamPlayer = $AudioStreamPlayer
						if qtd_acertos == opcoes_corretas.size() and qtd_erros == 0:
							audio_player.stream = _audio_acerto
						else:
							audio_player.stream = _audio_erro
						audio_player.play()
						yield(audio_player, 'finished')
						emit_signal("marcou")
						if mostrar_correcao_ao_concluir:
							_modo_correcao = true
						else:
							emit_signal("fechou")
					else:
						emit_signal("fechou")


func on_visibility_change():
	set_process(visible)

	if visible:
		limpar_resultado()
		_botao_ok.disabled = true

func _set_opcoes(op : PoolStringArray):
	limpar_resultado()
	opcoes = op
	_atualizar_opcoes()

func _set_opcoes_corretas(op : PoolStringArray):
	limpar_resultado()
	opcoes_corretas = op

func _set_opcoes_em_ordem_aleatoria(v : bool):
	if v == opcoes_em_ordem_aleatoria:
		return
	opcoes_em_ordem_aleatoria = v
	_atualizar_opcoes()

func limpar_resultado() -> void:
	opcoes_marcadas = []
	qtd_acertos = 0
	qtd_erros = 0
	percentual_acerto = 0.0

func _atualizar_opcoes() -> void:
	var opcoes_ordenadas : PoolStringArray
	
	# sorteia ou não a ordem das opções
	if not opcoes_em_ordem_aleatoria:
		opcoes_ordenadas = opcoes
	else:
		var ordem : Array = range(opcoes.size())
		ordem.shuffle()
		for i in ordem:
			opcoes_ordenadas.push_back(opcoes[i])
	
#	for no in _container.get_children():
#		no.queue_free()
#
#	# atualiza textos
#	for i in range(opcoes_ordenadas.size()):
#		var b : Button = Button.new()
#		b.text = opcoes_ordenadas[i]
#		_container.add_child(b)

	for i in range(opcoes_ordenadas.size()):
		var b : Button = _container.get_child(i)
		b.text = opcoes_ordenadas[i]
		b.pressed = false
	
	for i in range(opcoes_ordenadas.size(), _container.get_child_count()):
		var b : Button = _container.get_child(i)
		b.visible = false
	
	_botao_ok.disabled = true

func _atualizar_botao_ok():
	var qtd_opcoes_marcadas : int = 0
	for i in range(opcoes.size()):
		var b : Button = _container.get_child(i)
		if b.pressed:
			qtd_opcoes_marcadas += 1
	_botao_ok.disabled = qtd_opcoes_marcadas == 0

func _converter_y_para_viewport(p_y : float) -> float:
	var cubo : CubeMesh = _instancia.mesh
	var origem : Vector3 = _instancia.transform.origin
	var y0 : float = origem.y + cubo.size.y / 2
	var y1 : float = origem.y - cubo.size.y / 2
	var h : float = _viewport.size.y
	return (p_y * h - y0 * h)/(y1-y0)

func _esta_dentro_da_altura(p_y : float, p_c : Control) -> bool:
	return p_y > p_c.rect_global_position.y and p_y < p_c.rect_global_position.y + p_c.rect_size.y

func _computar_resultado() -> void:
	for botao in _container.get_children():
		if botao is Button and botao.pressed:
			opcoes_marcadas.push_back(botao.text)
			if botao.text in opcoes_corretas:
				qtd_acertos += 1
			else:
				qtd_erros += 1
	percentual_acerto = float(qtd_acertos - qtd_erros) / opcoes_corretas.size()

func _buscar_controle_vr(obj) -> ARVRController:
	if obj is ARVRController:
		return obj
	var pai = obj.get_parent()
	while pai and not (pai is ARVRController):
		pai = pai.get_parent()
	return pai

func _colore_a_borda(botao : Control, cor : Color):
	var nome_situacao : String = 'pressed' if botao.pressed else 'normal'
	var estilo = botao.get_stylebox(nome_situacao)
	var largura_borda : int = 5
	if estilo is StyleBoxFlat:
		estilo = estilo.duplicate()
		estilo.border_color = cor
		estilo.border_width_left = largura_borda
		estilo.border_width_right = largura_borda
		estilo.border_width_top = largura_borda
		estilo.border_width_bottom = largura_borda
	elif estilo is StyleBoxTexture:
		estilo = estilo.duplicate()
		var textura_original : Texture = estilo.texture
		var imagem : Image = textura_original.get_data()
		imagem = imagem.duplicate()
		imagem.lock()
		for x in range(0, largura_borda):
			for y in range(imagem.get_height()):
				imagem.set_pixel(x, y, cor)
		imagem.unlock()

		var textura : Texture = ImageTexture.new()
		textura.create(textura_original.get_width(), textura_original.get_height(), imagem.get_format(), textura_original.flags)
		textura.set_data(imagem)
		estilo.texture = textura
	elif estilo is StyleBoxLine:
		estilo = estilo.duplicate()
		estilo.color = cor
	else:
		estilo = null
		printerr('Estilo do botão não aceita mudar cor')
	if estilo:
		botao.add_stylebox_override(nome_situacao, estilo)

func _mostrar_correcao():
	for filho in _container.get_children():
		if filho is Button:
			if filho.text in opcoes_corretas:
				_colore_a_borda(filho, Color.green)
			else:
				_colore_a_borda(filho, Color.red)
	_botao_ok.text = 'Terminar revisão'

func _on_Viewport_size_changed():
	var malha : Mesh = _instancia.mesh
	var fator_de_tamanho : int = 1000 # Viewport -> mundo real
	var novo_tamanho : Vector2 = _viewport.size / fator_de_tamanho
	if malha is CubeMesh:
		malha.size.x = novo_tamanho.x
		malha.size.y = novo_tamanho.y
	elif malha is QuadMesh:
		malha.size = novo_tamanho
	else:
		printerr('forma de malha não conhecida para alterar tamanho no Painel de Opções')
	
	var forma_colisao : BoxShape = $"objeto/CollisionShape".shape
	if not forma_colisao:
		printerr('forma de colisão não conhecida para alterar tamanho no Painel de Opções')
	else:
		forma_colisao.extents.x = novo_tamanho.x / 2
		forma_colisao.extents.y = novo_tamanho.y / 2

func _recalcular_tamanho_viewport():
	var tamanho_viewport : Rect2
	for tela in _viewport.get_children():
		if tela.visible:
			tamanho_viewport = tamanho_viewport.merge(Rect2(Vector2(), tela.rect_position + tela.rect_size))
	_viewport.size = tamanho_viewport.size
