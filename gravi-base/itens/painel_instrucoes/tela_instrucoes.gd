extends Panel
class_name TelaInstrucoes

signal abriu
signal fechou
signal trocou_texto

enum Estado {Aberta, Fechada, Transicao}
var estado = Estado.Aberta

onready var no_texto_principal : Node = $"ColorRect/texto_principal"

enum Tipo {Normal, Erro}
var tipo = Tipo.Normal

export(Color) var cor_bordas_normal : Color = "2dba21"
export(Color) var cor_bordas_erro : Color = "d42e18"

onready var borda_topo : Node = $Line2D_topo
onready var borda_topo_quina : Node = $Line2D_topo_quina
onready var borda_base : Node = $Line2D_base
onready var borda_base_quina : Node = $Line2D_base_quina

var _texto_para_trocar = null

func _ready():
	estado = Estado.Fechada

func set_texto_principal(texto : String) -> void:
	if is_fechada():
		no_texto_principal.bbcode_text = texto
		emit_signal("trocou_texto")
	else:
		_texto_para_trocar = texto
		if not $AnimationPlayer.is_playing():
			ocultar_dica_como_fechar()
			$AnimationPlayer.play("TrocarTexto")

func _trocar_texto_principal() -> void:
	no_texto_principal.bbcode_text = _texto_para_trocar
	_texto_para_trocar = null

func set_tipo_instrucao(novo_tipo : int) -> void:
	self.tipo = novo_tipo

func abrir() -> void:
	if is_aberta():
		return
	match tipo:
		Tipo.Normal:
			_alterar_cor_bordas(cor_bordas_normal)
		Tipo.Erro:
			_alterar_cor_bordas(cor_bordas_erro)
	$AnimationPlayer.play("Aparecer")
	estado = Estado.Transicao

func fechar() -> void:
	if is_fechada():
		return
	$AnimationPlayer.play("Desaparecer")
	estado = Estado.Transicao

func is_aberta() -> bool:
	return estado == Estado.Aberta

func is_fechada() -> bool:
	return estado == Estado.Fechada

func exibir_dica_como_fechar(apenas_icone : bool = false) -> void:
	$Sprite.visible = true
	$feche_a_mao.visible = not apenas_icone

func ocultar_dica_como_fechar() -> void:
	$Sprite.visible = false
	$feche_a_mao.visible = false

func _alterar_cor_bordas(cor : Color) -> void:
	borda_topo.default_color = cor
	borda_topo_quina.default_color = cor
	borda_base.default_color = cor
	borda_base_quina.default_color = cor

func _on_AnimationPlayer_animation_finished(anim_name : String) -> void:
	match anim_name:
		"Aparecer":
			estado = Estado.Aberta
			emit_signal("abriu")
		"Desaparecer":
			estado = Estado.Fechada
			emit_signal("fechou")
		"TrocarTexto":
			emit_signal("trocou_texto")

	if _texto_para_trocar:
		if _texto_para_trocar != no_texto_principal.bbcode_text:
			set_texto_principal(_texto_para_trocar)
		else:
			_texto_para_trocar = null
