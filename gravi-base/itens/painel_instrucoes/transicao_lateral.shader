shader_type canvas_item;

uniform float limite_x = -50.0;
uniform float tamanho_rastro = 20.0;

varying vec2 vertice;

void vertex() {
	vertice = VERTEX;
}

void fragment() {
	float alpha = texture(TEXTURE, UV).a;
	if (limite_x > 0.0 && vertice.x < limite_x)
		COLOR.a = alpha * 0.9 * (1.0 - (limite_x - vertice.x) / tamanho_rastro);
	else if (limite_x < 0.0 && vertice.x > -limite_x)
		COLOR.a = alpha * (1.0 - (limite_x + vertice.x) / tamanho_rastro);
	else
		COLOR.a = alpha;
}