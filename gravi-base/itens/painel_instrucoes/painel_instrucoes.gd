extends Spatial

signal abriu
signal solicitou_fechar
signal fechou

export(bool) var sempre_mostrar_texto_como_fechar = false
export(bool) var ignora_usuario_fechar = false

var _pode_fechar = false
var _primeira_vez_visivel = true

export(float) var distancia_para_fechar : float = 2.0 setget _set_distancia_para_fechar
var _distancia2_para_fechar : float = 4.0
var _local_onde_abriu : Vector3

onready var tela_instrucoes : Node = get_node('viewport/tela_instrucoes')

func _ready():
	set_process(false)

func _process(_delta):
	if _pode_fechar:
		var fechamento_solicitado = false

		if _deveria_fechar_painel():
			fechamento_solicitado = true
		elif distancia_para_fechar > 0:
			var posicao_atual : Vector3 = get_parent().global_transform.origin
			if _local_onde_abriu.distance_squared_to(posicao_atual) > _distancia2_para_fechar:
				print_debug("andou demais com a janela aberta")
				fechamento_solicitado = true
		
		if fechamento_solicitado:
			_pode_fechar = false
			_primeira_vez_visivel = false
			emit_signal("solicitou_fechar")
			if not ignora_usuario_fechar:
				fechar()

func set_texto_principal(texto : String):
	set_process(false)
	tela_instrucoes.set_texto_principal(texto)

func set_tipo_instrucao(tipo : int):
	tela_instrucoes.set_tipo_instrucao(tipo)

func abrir() -> void:
	visible = true
	get_node('audio_abre').play()
	tela_instrucoes.ocultar_dica_como_fechar()
	tela_instrucoes.abrir()

func fechar() -> void:
	get_node('audio_fecha').play()
	tela_instrucoes.fechar()
	set_process(false)

func is_aberto() -> bool:
	return tela_instrucoes.is_aberta()

func is_fechado() -> bool:
	return tela_instrucoes.is_fechada()

func _deveria_fechar_painel():
	if Input.is_action_just_pressed('punho'):
		return true

	if Input.is_action_just_pressed('ui_accept'):
		return true

	return false

func _set_distancia_para_fechar(nova_distancia : float) -> void :
	distancia_para_fechar = nova_distancia
	_distancia2_para_fechar = nova_distancia * nova_distancia

func _on_timer_timeout():
	_pode_fechar = true
	var exibir_texto_tambem : bool = sempre_mostrar_texto_como_fechar or _primeira_vez_visivel
	tela_instrucoes.exibir_dica_como_fechar(exibir_texto_tambem)

func _on_tela_instrucoes_fechou():
	if not get_node("Timer").is_stopped():
		printerr("Timer deveria estar parado!")
	visible = false
	emit_signal("fechou")

func _on_tela_instrucoes_abriu():
	_local_onde_abriu = get_parent().global_transform.origin
	get_node('Timer').start()
	set_process(true)
	emit_signal("abriu")

func _on_tela_instrucoes_trocou_texto():
	if is_aberto():
		get_node('Timer').start()
		set_process(true)
